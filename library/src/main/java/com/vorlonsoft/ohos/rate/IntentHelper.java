package com.vorlonsoft.ohos.rate;

import java.util.ArrayList;
import java.util.Arrays;

import com.vorlonsoft.ohos.rate.annotation.NonNull;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;

import static com.vorlonsoft.ohos.rate.Constants.Utils.EMPTY_STRING_ARRAY;
import static com.vorlonsoft.ohos.rate.Constants.Utils.LOG_MESSAGE_PART_1;
import static com.vorlonsoft.ohos.rate.Constants.Utils.UTILITY_CLASS_MESSAGE;
import static com.vorlonsoft.ohos.rate.StoreType.AMAZON;
import static com.vorlonsoft.ohos.rate.StoreType.APPLE;
import static com.vorlonsoft.ohos.rate.StoreType.BAZAAR;
import static com.vorlonsoft.ohos.rate.StoreType.BLACKBERRY;
import static com.vorlonsoft.ohos.rate.StoreType.CHINESESTORES;
import static com.vorlonsoft.ohos.rate.StoreType.MI;
import static com.vorlonsoft.ohos.rate.StoreType.SAMSUNG;
import static com.vorlonsoft.ohos.rate.StoreType.SLIDEME;
import static com.vorlonsoft.ohos.rate.StoreType.TENCENT;
import static com.vorlonsoft.ohos.rate.StoreType.YANDEX;
import static com.vorlonsoft.ohos.rate.UriHelper.getStoreUri;
import static com.vorlonsoft.ohos.rate.Utils.isPackagesExists;

/**
 * <p>IntentHelper Class - intent helper class of the library.</p>
 */
final class IntentHelper {

    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00202, "RATE");

    private static final String AMAZON_APPSTORE_PACKAGE_NAME = "com.amazon.venezia";

    private static final String BLACKBERRY_WORLD_PACKAGE_NAME = "net.rim.bb.appworld";

    private static final String CAFE_BAZAAR_PACKAGE_NAME = "com.farsitel.bazaar";

    private static final String GOOGLE_PLAY_PACKAGE_NAME = "com.android.vending";

    private static final String MI_PACKAGE_NAME = "com.xiaomi.market";

    private static final String SAMSUNG_GALAXY_APPS_PACKAGE_NAME = "com.sec.android.app.samsungapps";

    private static final String SLIDEME_MARKETPLACE_PACKAGE_NAME = "com.slideme.sam.manager";

    private static final String TENCENT_PACKAGE_NAME = "com.tencent.android.qqdownloader";

    private static final String YANDEX_STORE_PACKAGE_NAME = "com.yandex.store";

    private static final String[] BROWSERS_PACKAGES_NAMES = {
            "com.android.chrome",
            "org.mozilla.firefox",
            "com.opera.browser",
            "com.opera.mini.native",
            "com.sec.android.app.sbrowser",
            "com.UCMobile.intl",
            "com.tencent.mtt",
            "com.android.browser"
    };

    private static final String[] CHINESE_STORES_PACKAGES_NAMES = {
            "com.tencent.android.qqdownloader", //腾讯应用宝
            "com.qihoo.appstore",               //360手机助手
            "com.xiaomi.market",                //小米应用商店
            "com.huawei.appmarket",             //华为应用商店
            "com.baidu.appsearch",              //百度手机助手
            "com.oppo.market",                  //OPPO应用商店
            "zte.com.market",                   //中兴应用商店
            "com.bbk.appstore",                 //VIVO应用商店
            "com.wandoujia.phoenix2",           //豌豆荚
            "com.pp.assistant",                 //PP手机助手
            "com.hiapk.marketpho",              //安智应用商店
            "com.dragon.android.pandaspace",    //91手机助手
            "com.yingyonghui.market",           //应用汇
            "com.tencent.qqpimsecure",          //QQ手机管家
            "com.mappn.gfan",                   //机锋应用市场
            "cn.goapk.market",                  //GO市场
            "com.yulong.android.coolmart",      //宇龙Coolpad应用商店
            "com.lenovo.leos.appstore",         //联想应用商店
            "com.coolapk.market"                //cool市场
    };

    private IntentHelper() {
        throw new UnsupportedOperationException("IntentHelper" + UTILITY_CLASS_MESSAGE);
    }

    private static boolean getHasWebUriIntentFlagForStore(final int appStore) {
        return (appStore != CHINESESTORES);
    }

    private static boolean getNeedStorePackageFlagForStore(final int appStore) {
        switch (appStore) {
            case CHINESESTORES:
            case SAMSUNG:
            case YANDEX:
                return true;
            default:
                return false;
        }
    }

    @NonNull
    private static String[] getPackagesNamesForStore(final int appStore) {
        switch (appStore) {
            case AMAZON:
                return new String[]{AMAZON_APPSTORE_PACKAGE_NAME};
            case APPLE:
                return EMPTY_STRING_ARRAY.toArray(new String[0]);
            case BAZAAR:
                return new String[]{CAFE_BAZAAR_PACKAGE_NAME};
            case BLACKBERRY:
                return new String[]{BLACKBERRY_WORLD_PACKAGE_NAME};
            case CHINESESTORES:
                return CHINESE_STORES_PACKAGES_NAMES;
            case MI:
                return new String[]{MI_PACKAGE_NAME};
            case SAMSUNG:
                return new String[]{SAMSUNG_GALAXY_APPS_PACKAGE_NAME};
            case SLIDEME:
                return new String[]{SLIDEME_MARKETPLACE_PACKAGE_NAME};
            case TENCENT:
                return new String[]{TENCENT_PACKAGE_NAME};
            case YANDEX:
                return new String[]{YANDEX_STORE_PACKAGE_NAME};
            default:
                return new String[]{GOOGLE_PLAY_PACKAGE_NAME};
        }
    }

    private static void setIntentForStore(final Intent intent) {
        intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
    }

    @NonNull
    static Intent[] createIntentsForStore(@NonNull final Context context, final int appStore,
                                          @NonNull final String paramName) {
        if (context == null) {
            HiLog.warn(label, LOG_MESSAGE_PART_1 + "can't check the availability of stores " +
                    "packages on the user device (context == null).");
            return new Intent[0];
        }

        final boolean needStorePackage = getNeedStorePackageFlagForStore(appStore);
        final boolean hasWebUriIntent = getHasWebUriIntentFlagForStore(appStore);
        final String[] storesPackagesNames = getPackagesNamesForStore(appStore);
        ArrayList<String> packNames = new ArrayList<>();
        for (String str : storesPackagesNames) {
            packNames.add(str);
        }
        final ArrayList<String> deviceStoresPackagesNames = isPackagesExists(context, packNames);
        final int deviceStoresPackagesNumber = deviceStoresPackagesNames.size();
        final Intent[] intents;

        Intent intent_action = new Intent();
        intent_action.setAction("android.intent.action.VIEW");
        Uri uri = Uri.parse(getStoreUri(appStore, paramName).toString());
        intent_action.setUri(uri);

        if (deviceStoresPackagesNumber > 0) {
            if (hasWebUriIntent) {
                intents = new Intent[deviceStoresPackagesNumber + 1];
                intents[deviceStoresPackagesNumber] = new Intent(intent_action);
            } else {
                intents = new Intent[deviceStoresPackagesNumber];
            }
            for (int b = 0; b < deviceStoresPackagesNumber; b++) {

                intents[b] = new Intent(intent_action);
                setIntentForStore(intents[b]);
                intents[b].setBundle(deviceStoresPackagesNames.get(b));
            }
        } else if (!needStorePackage) {
            intents = new Intent[]{new Intent(intent_action)};
            if (appStore == APPLE) {
                final ArrayList<String> deviceBrowsersPackagesNames = isPackagesExists(context,
                        (ArrayList<String>) Arrays.asList(BROWSERS_PACKAGES_NAMES));
                if (deviceBrowsersPackagesNames.size() > 0) {
                    intents[0].setBundle(deviceBrowsersPackagesNames.get(0));
                }
            }
        } else {
            intents = new Intent[0];
            if (hasWebUriIntent) {
                HiLog.warn(label, LOG_MESSAGE_PART_1 + Arrays.toString(storesPackagesNames) + " not " +
                        "exist on the user device and the user device can't start the app store (" +
                        appStore + ") web (http/https) uri activity without it.");
            } else {
                HiLog.warn(label, LOG_MESSAGE_PART_1 + Arrays.toString(storesPackagesNames) + " not " +
                        "exist on the user device and the app store (" + appStore +
                        ") hasn't web (http/https) uri.");
            }
        }
        return intents;
    }
}