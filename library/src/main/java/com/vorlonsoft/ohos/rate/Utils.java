package com.vorlonsoft.ohos.rate;

import ohos.app.Context;

import java.lang.reflect.Field;
import java.util.ArrayList;
import static com.vorlonsoft.ohos.rate.Constants.Utils.EMPTY_STRING_ARRAY;

public class Utils {

    public static RatingDialog getDialogBuilder(Context context, int themeResId) {
        return new RatingDialog(context);
    }

    public static RatingDialog getCommonDialog(Context context) {
        return new RatingDialog(context);
    }

    public static ArrayList<String> isPackagesExists(Context context, ArrayList<String> targetPackages) {
        ArrayList<String> storesPackagesName = new ArrayList<>();
        if (targetPackages.isEmpty()) {
            return EMPTY_STRING_ARRAY;
        } else {
            storesPackagesName.add("com.huawei.appmarket");
            return storesPackagesName;
        }
    }

    /**
     * 根据资源名称获取资源 id
     * <p>
     * <p>
     * 不提倡使用这个方法获取资源,比其直接获取ID效率慢
     * <p>
     * <p>
     * 例如
     * getResources().getIdentifier("ic_launcher", "drawable", getPackageName());
     *
     * @param name
     * @param defType
     * @return
     */

    public static int getResIdByName(String name, String defType) {
        String str =  name.split(":")[1];
        Field[] fields = ResourceTable.class.getDeclaredFields();
        for (Field field : fields) {
            String fieldName = field.getName().toLowerCase();
            int index = fieldName.indexOf("_");
            String type = fieldName.substring(0, index);
            String n = fieldName.substring(index + 1);
            if (type.equals(defType.toLowerCase()) && n.equals(str.toLowerCase())) {
                try {
                    int resId = field.getInt(ResourceTable.class);
                    return resId;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        }
        return -1;
    }
}
