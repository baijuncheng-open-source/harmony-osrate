package com.vorlonsoft.ohos.rate;

import ohos.app.Context;
import ohos.bundle.BundleInfo;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.rpc.RemoteException;

public class AppInformation {

    private static final String packageInfoException = "Failed to get app package info.";

    private static final String applicationIconException = "Failed to get app icon.";

    private static PixelMap icon = null;

    private static Long longVersionCode = null;

    private static String packageName = null;

    private static Integer versionCode = null;

    private static Integer versionCodeMajor = null;

    private static String versionName = null;

    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    public static PixelMap getIcon(Context context) {
        if (icon == null) {
            try {
                String str = context.getApplicationInfo().getIcon();
                int resId = new Utils().getResIdByName(str,"Media");
                ImageSource imageSource = ImageSource.create(context.getResourceManager().getResource(resId), null);
                icon = imageSource.createPixelmap(null);
            } catch (Exception e) {
                HiLog.info(label, applicationIconException, e);
            }
        }
        return icon;
    }

    public static int getLongVersionCode(Context context) {
        BundleInfo bundleInfo = null;
        try {
            bundleInfo = context.getBundleManager().getBundleInfo(context.getApplicationContext().getBundleName(), 0x00000000);
            return bundleInfo.getVersionCode();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getPackageName(Context context) {
        if (packageName == null)
            packageName = context.getApplicationContext().getBundleName();
        return packageName;
    }

    public static int getVersionCodeMajor(Context context) {
        if (versionCodeMajor == null)
            versionCodeMajor = getLongVersionCode(context) >>> 32;
        return versionCodeMajor;
    }

    public static String getVersionName(Context context) {
        if (versionName == null) {
            try {
                BundleInfo bundleInfo = null;
                bundleInfo = context.getBundleManager().getBundleInfo(context.getApplicationContext().getBundleName(), 0x00000000);
                versionName =  bundleInfo.getVersionName();
            } catch (Exception e) {
                HiLog.info(label, packageInfoException, e);
            }
        }
        return versionName;
    }
}
