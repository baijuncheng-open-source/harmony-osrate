package com.vorlonsoft.ohos.rate;

import com.vorlonsoft.ohos.rate.annotation.IntDef;

public class DialogType {

    @IntDef({APPLE, CLASSIC, MODERN})
    public @interface AnyDialogType {
    }

    /**
     * Apple Rate Dialog.
     */
    public static final int APPLE = 0;
    /**
     * Classic Rate Dialog.
     */
    public static final int CLASSIC = 1;
    /**
     * Modern Rate Dialog.
     */
    public static final int MODERN = 2;
}
