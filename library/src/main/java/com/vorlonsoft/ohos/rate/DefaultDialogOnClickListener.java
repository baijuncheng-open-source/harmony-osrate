package com.vorlonsoft.ohos.rate;

import java.lang.ref.WeakReference;
import com.vorlonsoft.ohos.rate.annotation.NonNull;
import com.vorlonsoft.ohos.rate.annotation.Nullable;
import ohos.aafwk.ability.AbilitySliceRuntimeException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.IDialog;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import static com.vorlonsoft.ohos.rate.Constants.Utils.EMPTY_STRING;
import static com.vorlonsoft.ohos.rate.Constants.Utils.LOG_MESSAGE_PART_1;
import static com.vorlonsoft.ohos.rate.DialogInterface.BUTTON_NEGATIVE;
import static com.vorlonsoft.ohos.rate.DialogInterface.BUTTON_NEUTRAL;
import static com.vorlonsoft.ohos.rate.DialogInterface.BUTTON_POSITIVE;
import static com.vorlonsoft.ohos.rate.IntentHelper.createIntentsForStore;
import static com.vorlonsoft.ohos.rate.PreferenceHelper.setIsAgreeShowDialog;
import static com.vorlonsoft.ohos.rate.PreferenceHelper.setRemindInterval;
import static com.vorlonsoft.ohos.rate.PreferenceHelper.setRemindLaunchesNumber;
import static com.vorlonsoft.ohos.rate.StoreType.AMAZON;
import static com.vorlonsoft.ohos.rate.StoreType.APPLE;
import static com.vorlonsoft.ohos.rate.StoreType.BAZAAR;
import static com.vorlonsoft.ohos.rate.StoreType.BLACKBERRY;
import static com.vorlonsoft.ohos.rate.StoreType.CHINESESTORES;
import static com.vorlonsoft.ohos.rate.StoreType.GOOGLEPLAY;
import static com.vorlonsoft.ohos.rate.StoreType.INTENT;
import static com.vorlonsoft.ohos.rate.StoreType.MI;
import static com.vorlonsoft.ohos.rate.StoreType.OTHER;
import static com.vorlonsoft.ohos.rate.StoreType.SAMSUNG;
import static com.vorlonsoft.ohos.rate.StoreType.SLIDEME;
import static com.vorlonsoft.ohos.rate.StoreType.TENCENT;
import static com.vorlonsoft.ohos.rate.StoreType.YANDEX;

/**
 * <p>DefaultDialogOnClickListener Class - the default Rate Dialog buttons on-click listener class
 * of the Rate library.</p>
 */
final class DefaultDialogOnClickListener implements Component.ClickedListener,
        DialogInterface.OnClickListener, IDialog.ClickedListener {
    /**
     * <p>The WeakReference to the {@link DefaultDialogOnClickListener} singleton object.</p>
     */
    private static volatile WeakReference<DefaultDialogOnClickListener> singleton = null;
    /**
     * <p>A Rate Dialog button callback specified by a library user.</p>
     */
    private OnClickButtonListener buttonListener;
    /**
     * <p>A activity context.</p>
     */
    private Context context;
    /**
     * <p>Store options.</p>
     */
    private final StoreOptions storeOptions;

    /**
     * <p>Creates {@link DefaultDialogOnClickListener} object.</p>
     *
     * @param buttonListener a Rate Dialog button callback specified by a library user
     * @param context        a activity context
     * @param storeOptions   store options
     */
    private DefaultDialogOnClickListener(@NonNull final Context context,
                                         @NonNull final StoreOptions storeOptions,
                                         @Nullable final OnClickButtonListener buttonListener) {
        this.context = context;
        this.storeOptions = storeOptions;
        this.buttonListener = buttonListener;
    }

    /**
     * <p>Creates the {@link DefaultDialogOnClickListener} singleton object.</p>
     *
     * @param context      a activity context
     * @param storeOptions store options
     * @return the {@link DefaultDialogOnClickListener} singleton object
     */
    @NonNull
    static DefaultDialogOnClickListener getInstance(@NonNull final Context context,
                                                    @NonNull final StoreOptions storeOptions,
                                                    @Nullable final OnClickButtonListener buttonListener) {
        if ((singleton == null) || (singleton.get() == null)) {
            synchronized (DefaultDialogOnClickListener.class) {
                if ((singleton == null) || (singleton.get() == null)) {
                    if (singleton != null) {
                        singleton.clear();
                    }
                    singleton = new WeakReference<>(new DefaultDialogOnClickListener(context,
                            storeOptions, buttonListener));
                } else {
                    singleton.get().context = context;
                    singleton.get().buttonListener = buttonListener;
                }
            }
        } else {
            singleton.get().context = context;
            singleton.get().buttonListener = buttonListener;
        }
        return singleton.get();
    }

    /**
     * <p>Called when a button has been clicked in a non-{@link DialogType#CLASSIC CLASSIC} Rate
     * Dialog.</p>
     */
    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00202, "RATE");

    @Override
    public void onClick(Component component) {
        AppRate.with(context).dismissRateDialog();
        final int buttonResId = component.getId();
        if (buttonResId == ResourceTable.Id_button_rate) {
            onClick((IDialog) null, BUTTON_POSITIVE);
        } else if (buttonResId == ResourceTable.Id_button_never) {
            onClick((IDialog) null, BUTTON_NEUTRAL);
        } else if (buttonResId == ResourceTable.Id_button_later) {
            onClick((IDialog) null, BUTTON_NEGATIVE);
        } else {
            HiLog.warn(label, LOG_MESSAGE_PART_1 + "dialog button with the given ResId doesn't " +
                    "exist.");
        }
    }

    @Override
    public void onClick(@Nullable final DialogInterface dialog, int which) {
        switch (which) {
            case BUTTON_POSITIVE:
                onPositiveButtonClick();
                break;
            case BUTTON_NEGATIVE:
                onNeutralButtonClick();
                break;
            case BUTTON_NEUTRAL:
                onNegativeButtonClick();
                break;
            default:
                HiLog.warn(label, LOG_MESSAGE_PART_1 + "dialog button with the given identifier " +
                        "doesn't exist.");
                return;
        }

        if (buttonListener != null) {
            buttonListener.onClickButton((byte) which);
        }
    }

    /**
     * <p>Calls when a positive button on the Rate Dialog is clicked.</p>
     */
    private void onPositiveButtonClick() {
        final String packageName = AppInformation.getPackageName(context);
        if (packageName.hashCode() != EMPTY_STRING.hashCode()) {
            final Intent[] intentsToAppStores = getIntentsForStores(packageName);
            try {
                if (intentsToAppStores.length == 0) {
                    HiLog.warn(label, LOG_MESSAGE_PART_1 + "no intent found for startActivity " +
                            "(intentsToAppStores.length == 0).");
                } else if (intentsToAppStores[0] == null) {
                    throw new AbilitySliceRuntimeException(LOG_MESSAGE_PART_1 + "no intent found for" +
                            " startActivity (intentsToAppStores[0] == null).");
                } else {
                    context.startAbilities(new Intent[]{intentsToAppStores[0]});
                }
            } catch (AbilitySliceRuntimeException e) {
                HiLog.warn(label, LOG_MESSAGE_PART_1 + "no activity found for " + intentsToAppStores[0], e);
                final byte intentsToAppStoresNumber = (byte) intentsToAppStores.length;
                if (intentsToAppStoresNumber > 1) {
                    boolean isCatch;
                    for (byte b = 1; b < intentsToAppStoresNumber; b++) {
                        try {
                            if (intentsToAppStores[b] == null) {
                                throw new AbilitySliceRuntimeException(LOG_MESSAGE_PART_1 +
                                        "no intent found for startActivity (intentsToAppStores[" +
                                        b + "] == null).");
                            } else {
                                context.startAbilities(new Intent[]{intentsToAppStores[b]});
                            }
                            isCatch = false;
                        } catch (AbilitySliceRuntimeException ex) {
                            HiLog.warn(label, LOG_MESSAGE_PART_1 + "no activity found for " +
                                    intentsToAppStores[b], ex);
                            isCatch = true;
                        }
                        if (!isCatch) {
                            break;
                        }
                    }
                }
            }
        } else {
            HiLog.warn(label, LOG_MESSAGE_PART_1 + "can't get app package name.");
        }
        setIsAgreeShowDialog(context, false);
    }

    /**
     * <p>Calls when a negative button on the Rate Dialog is clicked.</p>
     */
    private void onNegativeButtonClick() {
        setIsAgreeShowDialog(context, false);

    }

    /**
     * <p>Calls when a neutral button on the Rate Dialog is clicked.</p>
     */
    private void onNeutralButtonClick() {
        setRemindInterval(context);
        setRemindLaunchesNumber(context);
    }

    /**
     * <p>Creates intents for stores.</p>
     *
     * @param packageName package name
     * @return intents for stores
     */
    @NonNull
    private Intent[] getIntentsForStores(@NonNull final String packageName) {
        switch (storeOptions.getStoreType()) {
            case AMAZON:
                return createIntentsForStore(context, AMAZON, packageName);
            case APPLE:
                return createIntentsForStore(context, APPLE, storeOptions.getApplicationId());
            case BAZAAR:
                return createIntentsForStore(context, BAZAAR, packageName);
            case BLACKBERRY:
                return createIntentsForStore(context, BLACKBERRY, storeOptions.getApplicationId());
            case CHINESESTORES:
                return createIntentsForStore(context, CHINESESTORES, packageName);
            case MI:
                return createIntentsForStore(context, MI, packageName);
            case SAMSUNG:
                return createIntentsForStore(context, SAMSUNG, packageName);
            case SLIDEME:
                return createIntentsForStore(context, SLIDEME, packageName);
            case TENCENT:
                return createIntentsForStore(context, TENCENT, packageName);
            case YANDEX:
                return createIntentsForStore(context, YANDEX, packageName);
            case INTENT:
            case OTHER:
                return storeOptions.getIntents();
            default:
                return createIntentsForStore(context, GOOGLEPLAY, packageName);
        }
    }

    @Override
    public void onClick(IDialog iDialog, int i) {
        switch (i) {
            case BUTTON_POSITIVE:
                onPositiveButtonClick();
                break;
            case BUTTON_NEGATIVE:
                onNeutralButtonClick();
                break;
            case BUTTON_NEUTRAL:
                onNegativeButtonClick();
                break;
            default:
                HiLog.warn(label, LOG_MESSAGE_PART_1 + "dialog button with the given identifier " +
                        "doesn't exist.");
                return;
        }

        if (buttonListener != null) {
            buttonListener.onClickButton((byte) i);
        }
    }
}