package com.vorlonsoft.ohos.rate;

import ohos.agp.components.Button;
import ohos.agp.components.ComponentContainer;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

public class RatingDialog extends CommonDialog {

    private static Context context;
    private static DialogInterface.OnShowListener showListener;
    private static DialogInterface.OnDismissListener dismissListener;

    public RatingDialog(Context context) {
        super(context);
        this.context = context;
    }

    public static Context getContext() {
        return context;
    }


    public void setOnShowListener(DialogInterface.OnShowListener showListener) {
        this.showListener = showListener;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener dismissListener) {
        this.dismissListener = dismissListener;
    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    protected void onShow() {
        super.onShow();
        if (showListener != null) {
            showListener.onShow(this);
        }
    }

    @Override
    protected void onHide() {
        super.onHide();
        if (dismissListener != null) {
            dismissListener.onDismiss(this);
        }
    }

    public Button getButton(int id) {
        ComponentContainer container = (ComponentContainer) getButtonComponent();
        Button button = (Button) container.getComponentAt(id);
        return button;
    }
}
