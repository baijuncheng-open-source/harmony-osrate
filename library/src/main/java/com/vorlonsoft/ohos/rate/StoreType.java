package com.vorlonsoft.ohos.rate;
/*
 * All rights Reserved, Designed By www.bill-jc.com
 *
 * @Title: ${NetConnect}
 *
 * @Package ${coil.network}
 *
 * @Description: ${Perform web image loading)
 *
 * @author: baijuncheng
 *
 * @date: ${2021} ${3.8}
 *
 *  @version V1.1.1
 *
 * @Copyright: ${2021} bill-jc.com Inc. All rights reserved.
 */
import com.vorlonsoft.ohos.rate.annotation.IntDef;

public class StoreType {

    @IntDef({AMAZON, BAZAAR, CHINESESTORES, GOOGLEPLAY, MI, SAMSUNG, SLIDEME, TENCENT, YANDEX})
    public @interface StoreWithoutApplicationId {
    }

    @IntDef({APPLE, BLACKBERRY})
    public @interface StoreWithApplicationId {
    }

    @IntDef({AMAZON, APPLE, BAZAAR, BLACKBERRY, CHINESESTORES, GOOGLEPLAY, INTENT, MI, OTHER,
            SAMSUNG, SLIDEME, TENCENT, YANDEX})
    public @interface AnyStoreType {
    }

    /**
     * Amazon Appstore.
     */
    public static final int AMAZON = 0;
    /**
     * Apple App Store.
     */
    public static final int APPLE = 1;
    /**
     * Cafe Bazaar.
     */
    public static final int BAZAAR = 2;
    /**
     * BlackBerry World.
     */
    public static final int BLACKBERRY = 3;
    /**
     * 19 chinese app stores.
     */
    public static final int CHINESESTORES = 4;
    /**
     * Google Play.
     */
    public static final int GOOGLEPLAY = 5;
    /**
     * Mi Appstore (Xiaomi Market).
     */
    public static final int MI = 6;
    /**
     * Samsung Galaxy Apps.
     */
    public static final int SAMSUNG = 7;
    /**
     * SlideME Marketplace.
     */
    public static final int SLIDEME = 8;
    /**
     * Tencent App Store.
     */
    public static final int TENCENT = 9;
    /**
     * Yandex.Store.
     */
    public static final int YANDEX = 10;
    /**
     * Any custom intents.
     */
    public static final int INTENT = 11;
    /**
     * Any other Store.
     */
    public static final int OTHER = 12;
}
