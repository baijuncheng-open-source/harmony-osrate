package com.vorlonsoft.ohos.rate;

public interface OnClickButtonListener {

    public void onClickButton(Byte which);
}
