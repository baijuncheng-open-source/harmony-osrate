package com.vorlonsoft.ohos.rate;

import ohos.agp.window.dialog.BaseDialog;
import ohos.app.Context;

public interface DialogManager {

    BaseDialog createDialog();

    interface Factory {

        void clearDialogManager();

        DialogManager createDialogManager(Context context, DialogOptions dialogOptions, StoreOptions storeOptions);
    }
}
