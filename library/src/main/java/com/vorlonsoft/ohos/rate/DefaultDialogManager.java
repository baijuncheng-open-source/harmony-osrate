package com.vorlonsoft.ohos.rate;

import com.vorlonsoft.ohos.rate.annotation.NonNull;
import com.vorlonsoft.ohos.rate.annotation.Nullable;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Rating;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;
import java.lang.ref.WeakReference;

import static com.vorlonsoft.ohos.rate.DialogInterface.BUTTON_NEGATIVE;
import static com.vorlonsoft.ohos.rate.DialogInterface.BUTTON_NEUTRAL;
import static com.vorlonsoft.ohos.rate.DialogInterface.BUTTON_POSITIVE;
import static com.vorlonsoft.ohos.rate.DialogType.CLASSIC;
import static com.vorlonsoft.ohos.rate.PreferenceHelper.getDialogFirstLaunchTime;
import static com.vorlonsoft.ohos.rate.PreferenceHelper.increment365DayPeriodDialogLaunchTimes;
import static com.vorlonsoft.ohos.rate.PreferenceHelper.setDialogFirstLaunchTime;
import static com.vorlonsoft.ohos.rate.RatingDialog.getContext;
import static ohos.agp.components.Component.VERTICAL;
import static ohos.agp.utils.LayoutAlignment.END;

@SuppressWarnings("WeakerAccess")
public class DefaultDialogManager implements DialogManager {
    /**
     * <p>The WeakReference to the {@link DefaultDialogManager} singleton object.</p>
     */
    private static volatile WeakReference<DefaultDialogManager> singleton = null;
    @SuppressWarnings({"WeakerAccess", "UnusedAssignment"})
    protected DialogOptions dialogOptions = null;
    @SuppressWarnings({"WeakerAccess", "UnusedAssignment"})
    protected Context context = null;
    /**
     * <p>Listener used to allow to run some code when the Rate Dialog is shown.</p>
     */
    @SuppressWarnings("WeakerAccess")
    protected final DialogInterface.OnShowListener showListener = new DialogInterface.OnShowListener() {
        /**
         * <p>This method will be invoked when the Rate Dialog is shown.</p>
         *
         * @param dialog the Rate Dialog that was shown will be passed into the method
         */
        @Override
        public void onShow(RatingDialog dialog) {
            if (dialogOptions.getRedrawn()) {
                if (dialogOptions.getType() != CLASSIC) {
                    Rating rating = (Rating) ((DialogInterface) dialog).findComponentById(ResourceTable.Id_rate_dialog_rating_bar);
                    PixelMapElement element = null;
                    try {
                        element = new PixelMapElement(getContext().getResourceManager().getResource(ResourceTable.Media_rate_dialog_star_filled));
                        rating.setFilledElement(element);
                        PixelMapElement element1 = new PixelMapElement(getContext().getResourceManager().getResource(ResourceTable.Media_rate_dialog_star_unfilled));
                        rating.setUnfilledElement(element1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotExistException e) {
                        e.printStackTrace();
                    }
                    if (rating != null) {
                        rating.setScore(dialogOptions.getCurrentRating());
                    }
                }
            } else {
                if (getDialogFirstLaunchTime(context) == 0L) {
                    setDialogFirstLaunchTime(context);
                }
                increment365DayPeriodDialogLaunchTimes(context);
            }
            if ((dialog instanceof CommonDialog) &&
                    ((dialogOptions.getType() == CLASSIC) ||
                            (dialogOptions.getComponent(context) == null))) {
                try {
                    final Button positiveButton = (dialog).getButton(BUTTON_POSITIVE);
                    final DirectionalLayout directionallayout = (DirectionalLayout) positiveButton.getComponentParent();
                    if ((directionallayout.getOrientation() != VERTICAL) &&
                            ((positiveButton.getLeft() + positiveButton.getWidth()) >
                                    directionallayout.getWidth())) {
                        final Button neutralButton = (dialog
                                instanceof CommonDialog) ?
                                (dialog).getButton(BUTTON_NEUTRAL) :
                                (dialog).getButton(BUTTON_NEUTRAL);
                        final Button negativeButton = (dialog
                                instanceof CommonDialog) ?
                                (dialog).getButton(BUTTON_NEGATIVE) :
                                (dialog).getButton(BUTTON_NEGATIVE);
                        DirectionalLayout directionalLayout = new DirectionalLayout(context);
                        directionalLayout.setOrientation(VERTICAL);
                        directionalLayout.setAlignment(END);
                        if ((neutralButton != null) && (negativeButton != null)) {
                            directionalLayout.removeComponent(neutralButton);
                            directionalLayout.removeComponent(negativeButton);
                            directionalLayout.addComponent(negativeButton);
                            directionalLayout.addComponent(neutralButton);
                        } else if (neutralButton != null) {
                            directionalLayout.removeComponent(neutralButton);
                            directionalLayout.addComponent(neutralButton);
                        } else if (negativeButton != null) {
                            directionalLayout.removeComponent(negativeButton);
                            directionalLayout.addComponent(negativeButton);
                        }
                    }
                } catch (Exception e) {
                    final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00203, "RATE");
                    HiLog.info(label, "The Positive button may not fit in the window, can't check " +
                            "it and/or change the layout orientation to vertical if needed.");
                }
            }
        }
    };
    /**
     * <p>Listener used to allow to run some code when the Rate Dialog is dismissed.</p>
     */
    @SuppressWarnings("WeakerAccess")
    protected final DialogInterface.OnDismissListener dismissListener = new DialogInterface.OnDismissListener() {
        /**
         * <p>This method will be invoked when the Rate Dialog is dismissed.</p>
         *
         * @param dialog the Rate Dialog that was dismissed will be passed into the method
         */
        @Override
        public void onDismiss(RatingDialog dialog) {
            dialogOptions.setRedrawn(true);
        }
    };


    /**
     * <p>Listener used to allow to run some code when a button on the Rate Dialog is
     * clicked.</p>
     */
    @SuppressWarnings("WeakerAccess")
    protected final Component.ClickedListener buttonListener;
    /**
     * <p>Listener used to allow to run some code when a positive button on the Rate Dialog is
     * clicked.</p>
     */
    @SuppressWarnings("WeakerAccess")
    protected final DefaultDialogOnClickListener positiveListener;
    /**
     * <p>Listener used to allow to run some code when a negative button on the Rate Dialog is
     * clicked.</p>
     */
    @SuppressWarnings("WeakerAccess")
    protected final DefaultDialogOnClickListener negativeListener;
    /**
     * <p>Listener used to allow to run some code when a neutral button on the Rate Dialog is
     * clicked.</p>
     */
    @SuppressWarnings("WeakerAccess")
    protected final DefaultDialogOnClickListener neutralListener;

    @SuppressWarnings("WeakerAccess")
    protected DefaultDialogManager(final Context context, final DialogOptions dialogOptions,
                                   final StoreOptions storeOptions) {
        this.context = context;
        this.dialogOptions = dialogOptions;
        positiveListener = DefaultDialogOnClickListener.getInstance(context, storeOptions,
                dialogOptions.getButtonListener());
        negativeListener = positiveListener;
        neutralListener = positiveListener;
        buttonListener = positiveListener;
    }

    @SuppressWarnings("WeakerAccess")
    protected void setContext(Context context) {
        this.context = context;
    }

    /**
     * @param context    activity context
     * @param themeResId theme resource ID
     * @see AppCompatDialogManager#getAppCompatDialogBuilder(Context, int)
     */
    @NonNull
    protected RatingDialog getDialogBuilder(@NonNull final Context context,
                                            final int themeResId) {
        return Utils.getDialogBuilder(context, themeResId);
    }


    @SuppressWarnings("WeakerAccess")
    protected void supplyClassicDialogArguments(@NonNull CommonDialog builder,
                                                @NonNull Context dialogContext) {
        Component layout = LayoutScatter.getInstance(dialogContext).parse(ResourceTable.Layout_Layout_common_dialog_default, null, true);
        builder.setTransparent(true);
        builder.setContentCustomComponent(layout);
        Image icon = (Image) layout.findComponentById(ResourceTable.Id_dialog_image);
        Text title = (Text) layout.findComponentById(ResourceTable.Id_dialog_title_text);
        Text content = (Text) layout.findComponentById(ResourceTable.Id_dialog_content_text);
        Button later = (Button) layout.findComponentById(ResourceTable.Id_button_later);
        Button never = (Button) layout.findComponentById(ResourceTable.Id_button_never);
        Button rate = (Button) layout.findComponentById(ResourceTable.Id_button_rate);
        if (dialogOptions.isShowIcon()) {
            icon.setPixelMap(dialogOptions.getIcon(dialogContext));
        }
        if (dialogOptions.shouldShowTitle(dialogContext)) {
            title.setText(dialogOptions.getTitleText(dialogContext));
        }
        if (dialogOptions.shouldShowMessage()) {
            content.setText(dialogOptions.getMessageText(dialogContext));
        }
        if (dialogOptions.shouldShowNeutralButton()) {
            later.setText(dialogOptions.getNeutralText(dialogContext));
            later.setClickedListener(neutralListener);
        }
        if (dialogOptions.shouldShowNegativeButton()) {
            never.setText(dialogOptions.getNegativeText(dialogContext));
            never.setClickedListener(negativeListener);
        }
        rate.setText(dialogOptions.getPositiveText(dialogContext));
        rate.setClickedListener(positiveListener);
        builder.show();
    }

    /**
     * <p>Creates Rate Dialog.</p>
     *
     * @return created dialog
     */
    @SuppressWarnings("unused")
    @Nullable
    @Override
    public BaseDialog createDialog() {
        RatingDialog builder = getDialogBuilder(context, dialogOptions.getThemeResId());
        builder.setOnShowListener(showListener);
        builder.setOnDismissListener(dismissListener);
        Context dialogContext;
        dialogContext = context;
        final Component component = dialogOptions.getComponent(dialogContext);
        supplyClassicDialogArguments(builder, dialogContext);
        builder.setAutoClosable(dialogOptions.getCancelable(context));
        builder.setContentCustomComponent(component);
        builder.setDestroyedListener(new CommonDialog.DestroyedListener() {
            @Override
            public void onDestroy() {

            }
        });
        return builder;
    }

    /**
     * <p>DefaultDialogManager.Factory Class - default dialog manager factory class implements
     * {@link DialogManager.Factory} interface of the Rate library.</p>
     * <p>You can extend DefaultDialogManager.Factory Class and use
     * {@link AppRate#setDialogManagerFactory(DialogManager.Factory)} if you want to use fully
     * custom dialog (from v7 AppCompat library etc.).</p>
     *
     * @author Alexander Savin
     * @author Antoine Vianey
     * @version 1.2.1
     * @see DialogManager.Factory
     * @since 1.0.2
     */
    static class Factory implements DialogManager.Factory {

        Factory() {
            if (singleton != null) {
                singleton.clear();
            }
        }

        /**
         * <p>Clears {@link DefaultDialogManager} singleton.</p>
         */
        @SuppressWarnings("unused")
        @Override
        public void clearDialogManager() {
            if (singleton != null) {
                singleton.clear();
            }
        }

        /**
         * <p>Creates {@link DefaultDialogManager} singleton object.</p>
         *
         * @param context       activity context
         * @param dialogOptions Rate Dialog options
         * @param storeOptions  App store options
         * @return {@link DefaultDialogManager} singleton object
         */
        @SuppressWarnings("unused")
        @NonNull
        @Override
        public DialogManager createDialogManager(@NonNull final Context context,
                                                 @NonNull final DialogOptions dialogOptions,
                                                 @NonNull final StoreOptions storeOptions) {
            if ((singleton == null) || (singleton.get() == null)) {
                synchronized (DefaultDialogManager.class) {
                    if ((singleton == null) || (singleton.get() == null)) {
                        if (singleton != null) {
                            singleton.clear();
                        }
                        singleton = new WeakReference<>(new DefaultDialogManager(context,
                                dialogOptions, storeOptions));
                    } else {
                        singleton.get().setContext(context);
                    }
                }
            } else {
                singleton.get().setContext(context);
            }
            return singleton.get();
        }
    }
}