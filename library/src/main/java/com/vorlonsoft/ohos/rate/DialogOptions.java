package com.vorlonsoft.ohos.rate;

import com.vorlonsoft.ohos.rate.annotation.SuppressLint;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.lang.ref.SoftReference;

import static com.vorlonsoft.ohos.rate.DialogType.CLASSIC;

public class DialogOptions {


    public DialogOptions() {
    }

    HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "RATE");

    public Boolean cancelable = false;

    public Boolean getCancelable(Context context) {
        Preferences preferencesEditor = new DatabaseHelper(context).getPreferences("rate_pref_set_file");
        cancelable = preferencesEditor.getBoolean("Cancelable", false);
        return cancelable;
    }

    public void setCancelable(Boolean cancelable) {
        this.cancelable = cancelable;
    }

    public Boolean isRedrawn = false;

    public Boolean getRedrawn() {
        return isRedrawn;
    }

    public void setRedrawn(Boolean redrawn) {
        isRedrawn = redrawn;
    }

    public Boolean isShowIcon = null;

    public Boolean isShowIcon() {
        if (isShowIcon != null) {
            return isShowIcon;
        } else {
            return type != CLASSIC;
        }
    }

    public void setShowIcon(Boolean showIcon) {
        isShowIcon = showIcon;
    }

    public Boolean isShowMessage = true;

    @SuppressWarnings("unused")
    public Boolean shouldShowMessage() {
        return isShowMessage;
    }


    public void setShowMessage(Boolean showMessage) {
        isShowMessage = showMessage;
    }

    public Boolean isShowNegativeButton = true;

    @SuppressWarnings("unused")
    public Boolean shouldShowNegativeButton() {
        return isShowNegativeButton;
    }

    public void setShowNegativeButton(Boolean showNegativeButton) {
        isShowNegativeButton = showNegativeButton;
    }

    public Boolean isShowNeutralButton = true;

    @SuppressWarnings("unused")
    public Boolean shouldShowNeutralButton() {
        return isShowNeutralButton;
    }

    public void setShowNeutralButton(Boolean showNeutralButton) {
        isShowNeutralButton = showNeutralButton;
    }


    public Boolean isShowTitle = true;

    @SuppressWarnings("unused")
    public Boolean shouldShowTitle(Context context) {
        return isShowTitle;
    }

    public void setShowTitle(Boolean showTitle) {
        isShowTitle = showTitle;
    }

    public Byte currentRating = 0;

    public Byte getCurrentRating() {
        return currentRating;
    }

    public void setCurrentRating(Byte currentRating) {
        this.currentRating = currentRating;
    }

    public int iconResId = 0;

    public int getIconResId() {
        return iconResId;
    }

    public int messageTextResId = ResourceTable.String_rate_dialog_message;

    public int negativeTextResId = ResourceTable.String_rate_dialog_no;

    public int neutralTextResId = ResourceTable.String_rate_dialog_cancel;

    public void setNeutralTextResId(int neutralTextResId) {
        this.neutralTextResId = neutralTextResId;
    }

    public int positiveTextResId = ResourceTable.String_rate_dialog_ok;

    public Integer themeResId = null;

    public Integer getThemeResId() {
        if (themeResId != null) {
            return themeResId;
        } else if (type != CLASSIC) {
//             return ResourceTable.style.RateDialogTransparentTheme;
            return 0;
        } else {
            return 0;
        }
    }

    public void setThemeResId(Integer themeResId) {
        this.themeResId = themeResId;
    }

    public int titleTextResId = ResourceTable.String_rate_dialog_title;

    public void setTitleTextResId(int titleTextResId) {
        this.titleTextResId = titleTextResId;
    }

    @DialogType.AnyDialogType
    public int type = DialogType.CLASSIC;

    public int getType() {
        return type;
    }


    public void setType(int type) {
        this.type = type;
    }

    @SuppressWarnings("unused")
    @DialogType.AnyDialogType
    public int getDialogType() {
        return type;
    }

    public String messageText = null;

    public String getMessageText(Context context) {
        if (messageText == null) {
            return getMessageResourse(context, messageTextResId);
        } else {
            return messageText;
        }
    }

    public void setMessageTextResId(int messageTextResId) {
        this.messageTextResId = messageTextResId;
    }

    public String getMessageResourse(Context context, int ResId) {
        try {
            return context.getResourceManager().getElement(ResId).getString();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    private String negativeText = null;

    public String getNegativeText(Context context) {
        if (negativeText == null) {
            return getMessageResourse(context, negativeTextResId);
        } else {
            return negativeText;
        }
    }

    public void setNegativeTextResId(int negativeTextResId) {
        this.negativeTextResId = negativeTextResId;
    }

    public void setNegativeText(String negativeText) {
        this.negativeText = negativeText;
    }

    private String neutralText = null;

    public String getNeutralText(Context context) {
        if (neutralText == null) {
            return getMessageResourse(context, neutralTextResId);
        } else {
            return neutralText;
        }
    }

    public void setNeutralText(String neutralText) {
        this.neutralText = neutralText;
    }

    private String positiveText = null;

    public String getPositiveText(Context context) {
        if (positiveText == null) {
            return getMessageResourse(context, positiveTextResId);
        } else {
            return positiveText;
        }
    }

    public void setPositiveTextResId(int positiveTextResId) {
        this.positiveTextResId = positiveTextResId;
    }

    public void setPositiveText(String positiveText) {
        this.positiveText = positiveText;
    }

    private String titleText;

    public String getTitleText(Context context) {
        if (titleText == null) {
            return getMessageResourse(context, titleTextResId);
        } else {
            return titleText;
        }
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public SoftReference<OnClickButtonListener> buttonListener = null;

    public OnClickButtonListener getButtonListener() {
        if (buttonListener != null) {
            return buttonListener.get();
        }
        return null;
    }

    public void setButtonListener(OnClickButtonListener buttonListener) {
        this.buttonListener = new SoftReference(buttonListener);
    }

    private PixelMap icon = null;

    public PixelMap getIcon(Context context) {
        if (iconResId != 0) {
            try {
                ImageSource imageSource = ImageSource.create(context.getResourceManager().getResource(iconResId), null);
                icon = imageSource.createPixelmap(null);
            } catch (RuntimeException e) {
                HiLog.info(label, "Dialog icon with the given ResId doesn't exist.");
                if (icon != null) {
                    return icon;
                } else {
                    return AppInformation.getIcon(context);
                }
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (icon != null) {
                return icon;
            }
        } else {
            return AppInformation.getIcon(context);
        }
        return null;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }

    public void setIcon(PixelMap icon) {
        this.icon = icon;
    }

    @SuppressWarnings("unused")
    public PixelMap getDialogIcon(Context context) {
        return getIcon(context);
    }

    private Component component = null;

    public Component getComponent() {
        if (type != CLASSIC) {
            HiLog.warn(label, "For other than DialogType.CLASSIC dialog types call the" + "DialogOptions.getView(Context) instead of the DialogOptions.getView().");
        }
        return getComponent(null);
    }

    @SuppressLint("InflateParams")
    public Component getComponent(Context dialogContext) {
        if ((component == null) && (type != CLASSIC)) {
            Component dialogcomponent = null;
            if (dialogContext != null) {
                try {
                    dialogcomponent = LayoutScatter.getInstance(dialogContext).parse(ResourceTable.Layout_rate_dialog, null, false);
                } catch (AssertionError e) {
                    HiLog.info(label, "Can't inflate the R.layout.rate_dialog, layoutInflater not found," + "DialogType.CLASSIC dialog will be used.");
                }
            } else {
                HiLog.warn(label, "Can't inflate the R.layout.rate_dialog, dialogContext == null," + "DialogType.CLASSIC dialog will be used.");
            }
            return dialogcomponent;
        } else {
            return component;
        }
    }

    public void setComponent(Component component) {
        this.component = component;
    }
}
