package com.vorlonsoft.ohos.rate;

import com.vorlonsoft.ohos.rate.annotation.LongDef;

public class Time {

    @LongDef({MILLISECOND, SECOND, MINUTE, HOUR, DAY, WEEK, MONTH, YEAR})
    public @interface TimeUnits {
    }

    public static final long MILLISECOND = 1L;

    public static final long SECOND = MILLISECOND * 1_000L;

    public static final long MINUTE = SECOND * 60L;

    public static final long HOUR = MINUTE * 60L;

    public static final long DAY = HOUR * 24L;

    public static final long WEEK = DAY * 7L;

    public static final long MONTH = DAY * 30L;

    public static final long YEAR = DAY * 365L;
}
