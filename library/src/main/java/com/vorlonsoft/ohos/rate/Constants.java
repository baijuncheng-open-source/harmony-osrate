package com.vorlonsoft.ohos.rate;

import java.util.ArrayList;

public class Constants {

    public static class Date {
        public static final  Short YEAR_IN_DAYS = (short) 365;
    }

    static class Utils {
        public static final  String EMPTY_STRING = "";
        public static final  ArrayList<String> EMPTY_STRING_ARRAY = new ArrayList<>(0);
        public static final  String LOG_MESSAGE_PART_1 = "Failed to rate app, ";
        public static final  String TAG = "RATE";
        public static final  String UTILITY_CLASS_MESSAGE = " is a utility class and it can't be " +
                "instantiated!";
    }
}
