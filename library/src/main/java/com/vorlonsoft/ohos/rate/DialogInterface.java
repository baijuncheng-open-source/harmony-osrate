package com.vorlonsoft.ohos.rate;

import ohos.agp.components.Component;
import ohos.multimodalinput.event.KeyEvent;

public interface DialogInterface {

    int BUTTON_POSITIVE = -1;

    int BUTTON_NEGATIVE = -2;

    int BUTTON_NEUTRAL = -3;

    @Deprecated
    int BUTTON1 = BUTTON_POSITIVE;

    @Deprecated
    int BUTTON2 = BUTTON_NEGATIVE;

    @Deprecated
    int BUTTON3 = BUTTON_NEUTRAL;

    void cancel();

    void dismiss();

    Component findComponentById(int id);

    interface OnCancelListener {

        void onCancel(DialogInterface dialog);
    }

    interface OnDismissListener {

        void onDismiss(RatingDialog dialog);
    }

    interface OnShowListener {

        void onShow(RatingDialog dialog);
    }

    interface OnClickListener {

        void onClick(DialogInterface dialog, int which);
    }

    interface OnMultiChoiceClickListener {

        void onClick(DialogInterface dialog, int which, boolean isChecked);
    }

    interface OnKeyListener {

        boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event);
    }

}
