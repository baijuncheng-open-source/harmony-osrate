package com.vorlonsoft.ohos.rate;

import ohos.utils.net.Uri;

import static com.vorlonsoft.ohos.rate.StoreType.AMAZON;
import static com.vorlonsoft.ohos.rate.StoreType.APPLE;
import static com.vorlonsoft.ohos.rate.StoreType.BAZAAR;
import static com.vorlonsoft.ohos.rate.StoreType.BLACKBERRY;
import static com.vorlonsoft.ohos.rate.StoreType.CHINESESTORES;
import static com.vorlonsoft.ohos.rate.StoreType.MI;
import static com.vorlonsoft.ohos.rate.StoreType.SAMSUNG;
import static com.vorlonsoft.ohos.rate.StoreType.SLIDEME;
import static com.vorlonsoft.ohos.rate.StoreType.TENCENT;
import static com.vorlonsoft.ohos.rate.StoreType.YANDEX;

public class UriHelper {

    private static final String DEFAULT_STORE_URI = "market://details?id=";

    private static final String AMAZON_APPSTORE = "amzn://apps/android?p=";

    private static final String AMAZON_APPSTORE_WEB = "https://www.amazon.com/gp/mas/dl/android?p=";

    private static final String APPLE_APP_STORE_WEB = "https://itunes.apple.com/app/id";

    private static final String BLACKBERRY_WORLD = "appworld://content/";

    private static final String BLACKBERRY_WORLD_WEB = "https://appworld.blackberry.com/webstore/content/";

    private static final String CAFE_BAZAAR = "bazaar://details?id=";

    private static final String CAFE_BAZAAR_WEB = "https://cafebazaar.ir/app/";

    private static final String CHINESE_STORES = DEFAULT_STORE_URI;

    private static final String GOOGLE_PLAY = DEFAULT_STORE_URI;

    private static final String GOOGLE_PLAY_WEB = "https://play.google.com/store/apps/details?id=";

    private static final String MI_APPSTORE = DEFAULT_STORE_URI;

    private static final String MI_APPSTORE_WEB = "http://app.xiaomi.com/details?id=";

    private static final String SAMSUNG_GALAXY_APPS = "samsungapps://ProductDetail/";

    private static final String SAMSUNG_GALAXY_APPS_WEB =
            "https://apps.samsung.com/appquery/appDetail.as?appId=";

    private static final String SLIDEME_MARKETPLACE = "sam://details?id=";

    private static final String SLIDEME_MARKETPLACE_WEB = "http://slideme.org/app/";

    private static final String TENCENT_APP_STORE = DEFAULT_STORE_URI;

    private static final String TENCENT_APP_STORE_WEB = "http://a.app.qq.com/o/simple.jsp?pkgname=";

    private static final String YANDEX_STORE = "yastore://details?id=";

    private static final String YANDEX_STORE_WEB = "https://store.yandex.com/apps/details?id=";

    private static final Uri formUri(String part1, String part2) {
        return Uri.parse(part1 + part2);
    }

    public static Uri getStoreUri(int appStore, String paramName) {
        switch (appStore) {
            case AMAZON:
                return formUri(AMAZON_APPSTORE, paramName);
            case APPLE:
                return null;
            case BAZAAR:
                return formUri(CAFE_BAZAAR, paramName);
            case BLACKBERRY:
                return formUri(BLACKBERRY_WORLD, paramName);
            case CHINESESTORES:
                return formUri(CHINESE_STORES, paramName);
            case MI:
                return formUri(MI_APPSTORE, paramName);
            case SAMSUNG:
                return formUri(SAMSUNG_GALAXY_APPS, paramName);
            case SLIDEME:
                return formUri(SLIDEME_MARKETPLACE, paramName);
            case TENCENT:
                return formUri(TENCENT_APP_STORE, paramName);
            case YANDEX:
                return formUri(YANDEX_STORE, paramName);
            default:
                return formUri(GOOGLE_PLAY, paramName);
        }
    }

    public static Uri getStoreWebUri(int appStore, String paramName) {
        switch (appStore) {
            case AMAZON:
                return formUri(AMAZON_APPSTORE_WEB, paramName);
            case APPLE:
                return formUri(APPLE_APP_STORE_WEB, paramName);
            case BAZAAR:
                return formUri(CAFE_BAZAAR_WEB, paramName);
            case BLACKBERRY:
                return formUri(BLACKBERRY_WORLD_WEB, paramName);
            case CHINESESTORES:
                return null;
            case MI:
                return formUri(MI_APPSTORE_WEB, paramName);
            case SAMSUNG:
                return formUri(SAMSUNG_GALAXY_APPS_WEB, paramName);
            case SLIDEME:
                return formUri(SLIDEME_MARKETPLACE_WEB, paramName);
            case TENCENT:
                return formUri(TENCENT_APP_STORE_WEB, paramName);
            case YANDEX:
                return formUri(YANDEX_STORE_WEB, paramName);
            default:
                return formUri(GOOGLE_PLAY_WEB, paramName);
        }
    }
}
