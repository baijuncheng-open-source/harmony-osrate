package com.vorlonsoft.ohos.rate;

import com.vorlonsoft.ohos.rate.annotation.NonNull;
import com.vorlonsoft.ohos.rate.annotation.NotNull;
import com.vorlonsoft.ohos.rate.annotation.Nullable;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.Button;
import ohos.agp.window.dialog.BaseDialog;
import ohos.app.Context;
import java.lang.ref.WeakReference;
import ohos.agp.window.dialog.CommonDialog;

public class AppCompatDialogManager extends DefaultDialogManager implements DialogManager {
    /**
     * <p>The WeakReference to the {@link AppCompatDialogManager} singleton object.</p>
     */
    private static volatile WeakReference<DialogManager> singleton = null;

    @SuppressWarnings("WeakerAccess")
    protected AppCompatDialogManager(final Context context, final DialogOptions dialogOptions,
                                     final StoreOptions storeOptions) {
        super(context, dialogOptions, storeOptions);
    }

    @SuppressWarnings("WeakerAccess")
    @NonNull
    protected RatingDialog getAppCompatDialogBuilder(@NonNull final Context context,
                                                     final int themeResId) {
        return Utils.getCommonDialog(context);
    }

    @SuppressWarnings("WeakerAccess")
    protected void supplyAppCompatClassicDialogArguments(@NonNull CommonDialog builder, @NonNull Context dialogContext) {
        Component layout = LayoutScatter.getInstance(dialogContext).parse(ResourceTable.Layout_Layout_common_dialog_default, null, true);
        builder.setTransparent(true);
        builder.setContentCustomComponent(layout);
        Image icon = (Image) layout.findComponentById(ResourceTable.Id_dialog_image);
        Text title = (Text) layout.findComponentById(ResourceTable.Id_dialog_title_text);
        Text content = (Text) layout.findComponentById(ResourceTable.Id_dialog_content_text);
        Button later = (Button) layout.findComponentById(ResourceTable.Id_button_later);
        Button never = (Button) layout.findComponentById(ResourceTable.Id_button_never);
        Button rate = (Button) layout.findComponentById(ResourceTable.Id_button_rate);
        builder.show();
        if (dialogOptions.isShowIcon()) {
            icon.setPixelMap(dialogOptions.getIconResId());
        }
        if (dialogOptions.shouldShowTitle(dialogContext)) {
            title.setText(dialogOptions.getTitleText(dialogContext));
        }
        if (dialogOptions.shouldShowMessage()) {
            content.setText(dialogOptions.getMessageText(dialogContext));
        }
        if (dialogOptions.shouldShowNeutralButton()) {
            later.setText(dialogOptions.getNeutralText(dialogContext));
            later.setClickedListener(neutralListener);
        }
        if (dialogOptions.shouldShowNegativeButton()) {
            never.setText(dialogOptions.getNegativeText(dialogContext));
            never.setClickedListener(negativeListener);
        }
        rate.setText(dialogOptions.getPositiveText(dialogContext));
        rate.setClickedListener(positiveListener);
    }

    /**
     * <p>Creates Rate Dialog.</p>
     *
     * @return created dialog
     */
    @Nullable
    @Override
    public BaseDialog createDialog() {

        RatingDialog builder = getAppCompatDialogBuilder(context, dialogOptions.getThemeResId());
        builder.setOnShowListener(showListener);
        builder.setOnDismissListener(dismissListener);
        Context dialogContext = builder.getContext();

        final Component component = dialogOptions.getComponent(dialogContext);

        supplyAppCompatClassicDialogArguments(builder, dialogContext);
        /*if ((dialogOptions.getType() == CLASSIC) || (component == null)) {
            if (dialogOptions.getType() != CLASSIC) {
                builder = getAppCompatDialogBuilder(context, 0);
                dialogContext = builder.getContext();
            }
            supplyAppCompatClassicDialogArguments(builder, dialogContext);
        } else {
            supplyNonClassicDialogArguments(component, dialogContext);
        }
*/
        builder.setAutoClosable(dialogOptions.getCancelable(context));
        builder.setContentCustomComponent(component);
        builder.setDestroyedListener(new CommonDialog.DestroyedListener() {
            @Override
            public void onDestroy() {

            }
        });


        return builder;
    }

    /**
     * <p>AppCompatDialogManager.Factory Class - v7 AppCompat library dialog manager factory class
     * implements {@link DialogManager.Factory} interface of the  library.</p>
     * <p>You can extend AppCompatDialogManager.Factory Class and use
     * {@link AppRate#setDialogManagerFactory(DialogManager.Factory)} if you want to use fully
     * custom dialog (from v7 AppCompat library).</p>
     *
     * @author Alexander Savin
     * @version 1.2.1
     * @see DialogManager.Factory
     * @since 1.2.1
     */
    public static class Factory implements DialogManager.Factory {

        public Factory() {
            if (singleton != null) {
                singleton.clear();
            }
        }

        /**
         * <p>Clear {@link AppCompatDialogManager} singleton.</p>
         */
        @SuppressWarnings("unused")
        @Override
        public void clearDialogManager() {
            if (singleton != null) {
                singleton.clear();
            }
        }

        /**
         * <p>Creates {@link AppCompatDialogManager} singleton object.
         * <b>{@link DefaultDialogManager} singleton object will be created for API levels 13 and
         * below instead</b></p>
         *
         * @param context       context
         * @param dialogOptions Rate Dialog options
         * @param storeOptions  App store options
         * @return {@link AppCompatDialogManager} singleton object for API levels 14 and higher,
         * <b>{@link DefaultDialogManager} singleton object for API levels 13 and below</b>
         */
        @SuppressWarnings("unused")
        @NotNull
        @Override
        public DialogManager createDialogManager(@NotNull final Context context,
                                                 @NotNull final DialogOptions dialogOptions,
                                                 @NotNull final StoreOptions storeOptions) {
            if ((singleton == null) || (singleton.get() == null)) {
                synchronized (AppCompatDialogManager.class) {
                    if ((singleton == null) || (singleton.get() == null)) {
                        if (singleton != null) {
                            singleton.clear();
                        }
                        singleton = new WeakReference<>(new AppCompatDialogManager(context, dialogOptions, storeOptions));
                    } else {
                        ((DefaultDialogManager) singleton.get()).setContext(context);
                    }
                }
            } else {
                ((DefaultDialogManager) singleton.get()).setContext(context);
            }
            return singleton.get();
        }
    }
}