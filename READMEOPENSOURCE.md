[
    {
        "Name": "HarmonyOSRate",
        "License": "Apache License Version 2.0",
        "License File": "LICENSE.txt",
        "Version Number": "1.0",
        "Upstream URL": "https://github.com/Vorlonsoft/AndroidRate",
        "Description": "A library for rate library for HarmonyOS"
    }
]