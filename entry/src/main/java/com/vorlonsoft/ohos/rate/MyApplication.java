package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.AbilityPackage;
import ohos.os.ProcessManager;

public class MyApplication extends AbilityPackage {

    public MyApplication() {
        super();
    }

    @Override
    public void onInitialize() {
        super.onInitialize();
    }

    @Override
    public void onEnd() {
        ProcessManager.kill(ProcessManager.getPid());
        super.onEnd();
    }
}
