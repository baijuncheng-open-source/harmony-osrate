package com.vorlonsoft.ohos.rate.slice;

import com.vorlonsoft.ohos.rate.AppRate;
import com.vorlonsoft.ohos.rate.OnClickButtonListener;
import com.vorlonsoft.ohos.rate.ResourceTable;
import com.vorlonsoft.ohos.rate.StoreType;
import com.vorlonsoft.ohos.rate.Time;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

import static com.vorlonsoft.ohos.rate.DialogInterface.BUTTON_NEUTRAL;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    private Preferences preferencesEditor;
    private Preferences preferencesEditors;
    private ToastDialog toastDialog;
    private String s;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        toastDialog = new ToastDialog(this);

        preferencesEditor = new DatabaseHelper(this).getPreferences("rate_pref_set_file");
        preferencesEditors = new DatabaseHelper(this).getPreferences("rate_pref_file");
        preferencesEditors.putString("LongVersionCode1", "LongVersionCode1");
        Button button1 = (Button) findComponentById(ResourceTable.Id_Cancelable);
        Button button2 = (Button) findComponentById(ResourceTable.Id_selectedAppLaunches);
        Button button3 = (Button) findComponentById(ResourceTable.Id_Title);
        Button button4 = (Button) findComponentById(ResourceTable.Id_Hide_Title);
        Button button5 = (Button) findComponentById(ResourceTable.Id_Context);
        Button button6 = (Button) findComponentById(ResourceTable.Id_HIEDCONTEXT);
        Button button7 = (Button) findComponentById(ResourceTable.Id_BUTTON);
        Button button8 = (Button) findComponentById(ResourceTable.Id_HIED_BUTTON);
        Button button9 = (Button) findComponentById(ResourceTable.Id_Icon);
        Button button10 = (Button) findComponentById(ResourceTable.Id_HIDE_ICON);
        Button button11 = (Button) findComponentById(ResourceTable.Id_setDebug);
        Button button12 = (Button) findComponentById(ResourceTable.Id_SHOW_CONTENT);
        Button button13 = (Button) findComponentById(ResourceTable.Id_SHOW_TITLE);
        Button button14 = (Button) findComponentById(ResourceTable.Id_Clear_set_Param);
        Button button15 = (Button) findComponentById(ResourceTable.Id_get_Pre);
        Button button16 = (Button) findComponentById(ResourceTable.Id_time_to_wait);
        button1.setClickedListener(this);
        button2.setClickedListener(this);
        button3.setClickedListener(this);
        button4.setClickedListener(this);
        button5.setClickedListener(this);
        button6.setClickedListener(this);
        button7.setClickedListener(this);
        button8.setClickedListener(this);
        button9.setClickedListener(this);
        button10.setClickedListener(this);
        button11.setClickedListener(this);
        button12.setClickedListener(this);
        button13.setClickedListener(this);
        button14.setClickedListener(this);
        button15.setClickedListener(this);
        button16.setClickedListener(this);
        show();
    }

    private void show() {
        AppRate.with(this)
                .setStoreType(StoreType.CHINESESTORES)
                .setTimeToWait(Time.DAY, (short) 0)
                .setLaunchTimes((byte) 0)
                .setShowDialogIcon(true)
                .set365DayPeriodMaxNumberDialogLaunchTimes((short) 3)
                .setOnClickButtonListener(new OnClickButtonListener() {
                    @Override
                    public void onClickButton(Byte which) {
                        switch (which) {
                            case BUTTON_NEUTRAL:
                                toastDialog.setText("点击了不，谢谢").setDuration(2000).show();
                        }
                    }
                })
                .monitor();

        AppRate.showRateDialogIfMeetsConditions(this);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
    }

    @Override
    public void onActive() {
        super.onActive();

    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_setDebug:
                AppRate.with(this).setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_Cancelable:
                preferencesEditor.putBoolean("Cancelable", true).flush();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_selectedAppLaunches:
                AppRate.with(this).setSelectedAppLaunches((byte) 2).monitor();
                preferencesEditor.putInt("setSelectedAppLaunches", 2).flush();
                break;
            case ResourceTable.Id_Title:
                AppRate.with(this).setTitle("hello world").setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_Hide_Title:
                AppRate.with(this).setShowTitle(false).setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_Context:
                AppRate.with(this).setMessage("hello world").setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_HIEDCONTEXT:
                AppRate.with(this).setShowMessage(false).setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_BUTTON:
                AppRate.with(this).setTextNever("hello world").setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_HIED_BUTTON:
                AppRate.with(this).setShowLaterButton(false).setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_Icon:
                AppRate.with(this)
                        .setDialogIcon(ResourceTable.Media_rate_dialog_star_unfilled)
                        .setShowDialogIcon(true)
                        .setDebug(true);
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_HIDE_ICON:
                AppRate.with(this).setShowDialogIcon(false).setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_SHOW_CONTENT:
                AppRate.with(this).setShowMessage(true).setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_SHOW_TITLE:
                AppRate.with(this).setShowTitle(true).setDebug(true).monitor();
                AppRate.showRateDialogIfMeetsConditions(this);
                break;
            case ResourceTable.Id_Clear_set_Param:
                AppRate.with(this).clearSettingsParam();
                s = preferencesEditors.getString("LongVersionCode1", "");
                toastDialog.setText("s =" + s + "SettingsParam is cleared").setDuration(3000).show();
                break;
            case ResourceTable.Id_get_Pre:
                s = preferencesEditors.getString("LongVersionCode1", "");
                toastDialog.setText("s =" + s).setDuration(3000).show();
                break;
            case ResourceTable.Id_time_to_wait:
                AppRate.with(this).setRemindTimeToWait(Time.DAY, (short) 2).monitor();
                preferencesEditor.putInt("setRemindTimeToWait", 2).flush();
                break;
        }
    }
}
