package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.window.dialog.BaseDialog;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * create date：2021/6/8 11:07
 *
 * @author huxi
 * description：
 */
public class DefaultDialogManagerTest {

    private Context context;
    private DialogOptions dialogOptions =  new DialogOptions();
    private StoreOptions storeOptions =  new StoreOptions();
    private DefaultDialogManager defaultDialogManager ;
    private MainAbility ability;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.vorlonsoft.ohos.rate")
                .withAbilityName("com.vorlonsoft.ohos.rate.MainAbility")
                .build();
        intent.setOperation(operation);
        ability = (MainAbility) AbilityDelegatorRegistry.getAbilityDelegator().startAbilitySync(intent).get();
        defaultDialogManager = new DefaultDialogManager(context, dialogOptions, storeOptions);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void setContext() {
       defaultDialogManager.setContext(context);
        Context contexts = defaultDialogManager.context;
        Assert.assertTrue(contexts == context);
    }

    @Test
    public void getDialogBuilder() {
        int i = ResourceTable.Id_dialog_title_text;
        RatingDialog dialogBuilder = defaultDialogManager.getDialogBuilder(context, i);
        Assert.assertTrue(dialogBuilder != null );
    }

    @Test
    public void supplyClassicDialogArguments() {
        RatingDialog ratingDialog = new RatingDialog(ability);
        dialogOptions.isShowIcon = true;
        dialogOptions.isShowTitle = true;
        dialogOptions.isShowMessage = true;
        dialogOptions.isShowNeutralButton = true;
        defaultDialogManager.supplyClassicDialogArguments(ratingDialog, ability);
        Assert.assertTrue(ratingDialog.isShowing());
    }

    @Test
    public void createDialog() {
        defaultDialogManager.setContext(ability);
        BaseDialog dialog = defaultDialogManager.createDialog();
        Assert.assertNotNull(dialog);
    }
}