package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.data.preferences.Preferences;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

/**
 * create date：2021/6/8 11:10
 *
 * @author huxi
 * description：
 */
public class PreferenceHelperTest {

    private Context context;
    private Preferences preferences;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        preferences = PreferenceHelper.getPreferences(context);
    }

    @After
    public void tearDown() throws Exception {
        preferences.clear();
    }

    @Test
    public void getPreferences() {
        Preferences preferences = PreferenceHelper.getPreferences(context);
        Assert.assertNotNull(preferences);
    }

    @Test
    public void clearSharedPreferences() {
        PreferenceHelper.clearSharedPreferences(context);
        int remindLaunchesNumber = preferences.getInt("rate_remind_launches_number", 0);
        Assert.assertTrue(remindLaunchesNumber == 0);
    }

    @Test
    public void isFirstLaunch() {
        boolean firstLaunch = PreferenceHelper.isFirstLaunch(context);
        Assert.assertTrue(firstLaunch == true);
    }

    @Test
    public void setFirstLaunchSharedPreferences() {
        PreferenceHelper.setFirstLaunchSharedPreferences(context);
        int launchTimes = preferences.getInt("rate_launch_times", 0);
        boolean pref_key_is_agree_show_dialog = preferences.getBoolean("PREF_KEY_IS_AGREE_SHOW_DIALOG", true);
        Assert.assertTrue(launchTimes == 1);
        Assert.assertTrue(pref_key_is_agree_show_dialog == true);
    }

    @Test
    public void increment365DayPeriodDialogLaunchTimes() {
    }

    @Test
    public void get365DayPeriodDialogLaunchTimes() {
        short dayPeriodDialogLaunchTimes = PreferenceHelper.get365DayPeriodDialogLaunchTimes(context);
        Assert.assertTrue(dayPeriodDialogLaunchTimes == 0);
    }

    @Test
    public void setCustomEventCount() {
        String eventName = "name";
        short eventCount = 1;
        PreferenceHelper.setCustomEventCount(context,eventName,eventCount);
        int customEventPrefixName = preferences.getInt("rate_custom_event_prefix_name", eventCount);
        Assert.assertTrue(customEventPrefixName == 1);
    }

    @Test
    public void getCustomEventCount() {
        String eventName = "name";
        PreferenceHelper.getCustomEventCount(context,eventName);
        int customEventPrefixName = preferences.getInt("rate_custom_event_prefix_name", 0);
        Assert.assertTrue(customEventPrefixName == 0);
    }

    @Test
    public void setDialogFirstLaunchTime() {
        long time = new Date().getTime();
        PreferenceHelper.setDialogFirstLaunchTime(context);
        long dialogFirstlaunchtime = preferences.getLong("rate_dialog_first_launch_time", 0l);
        Assert.assertTrue(dialogFirstlaunchtime == time);
    }

    @Test
    public void getDialogFirstLaunchTime() {
        PreferenceHelper.getDialogFirstLaunchTime(context);
        long dialogFirstLaunchTime = preferences.getLong("rate_dialog_first_launch_time", 0l);
        Assert.assertTrue(dialogFirstLaunchTime == 0);
    }

    @Test
    public void getInstallDate() {
        PreferenceHelper.getInstallDate(context);
        long installDate = preferences.getLong("rate_install_date_", 0l);
        Assert.assertTrue(installDate == 0);
    }

    @Test
    public void setIsAgreeShowDialog() {
        boolean isAgree = true;
        PreferenceHelper.setIsAgreeShowDialog(context,isAgree);
        boolean isAgreeShowDialog = preferences.getBoolean("rate_is_agree_show_dialog", true);
        Assert.assertTrue(isAgreeShowDialog == true);
    }

    @Test
    public void getIsAgreeShowDialog() {
        PreferenceHelper.getIsAgreeShowDialog(context);
        boolean is_agree_show_dialog = preferences.getBoolean("rate_is_agree_show_dialog", true);
        Assert.assertTrue(is_agree_show_dialog == true);
    }

    @Test
    public void setLaunchTimes() {
        short launchTimes = 3;
        PreferenceHelper.setLaunchTimes(context,launchTimes);
        int launch_times = preferences.getInt("rate_launch_times", 0);
        Assert.assertTrue(launch_times == 3);
    }

    @Test
    public void getLaunchTimes() {
        short launchTimes = PreferenceHelper.getLaunchTimes(context);
        Assert.assertTrue(launchTimes == 0);
    }

    @Test
    public void setRemindInterval() {
        long time = new Date().getTime();
        PreferenceHelper.setRemindInterval(context);
        long remind_interval = preferences.getLong("rate_remind_interval", 0l);
        Assert.assertTrue(remind_interval == time);
    }

    @Test
    public void getRemindInterval() {
        PreferenceHelper.getRemindInterval(context);
        long remind_interval = preferences.getLong("rate_remind_interval", 0L);
        Assert.assertTrue(remind_interval == 0);
    }

    @Test
    public void setRemindLaunchesNumber() {
        PreferenceHelper.setRemindLaunchesNumber(context);
        int remind_launches_number = preferences.getInt("rate_remind_launches_number", 0);
        Assert.assertTrue(remind_launches_number == 0);
    }

    @Test
    public void getRemindLaunchesNumber() {
        short remindLaunchesNumber = PreferenceHelper.getRemindLaunchesNumber(context);
        Assert.assertTrue(remindLaunchesNumber == 0);
    }

    @Test
    public void clearRemindButtonClick() {
        PreferenceHelper.clearRemindButtonClick(context);
        long remind_interval = preferences.getLong("rate_remind_interval", 0);
        int launches_number = preferences.getInt("rate_remind_launches_number", 0);
        Assert.assertTrue(remind_interval == 0);
        Assert.assertTrue(launches_number == 0);
    }

    @Test
    public void setVersionCode() {
        int longVersionCode = AppInformation.getLongVersionCode(context);
        PreferenceHelper.setVersionCode(context);
        long version_code = preferences.getLong("rate_version_code", 0l);
        Assert.assertTrue(version_code == longVersionCode);
    }

    @Test
    public void getVersionCode() {
        long versionCode = PreferenceHelper.getVersionCode(context);
        Assert.assertTrue(versionCode == 0);
    }

    @Test
    public void setVersionName() {
        String versionName1 = AppInformation.getVersionName(context);
        PreferenceHelper.setVersionName(context);
        String versionName = preferences.getString("rate_version_name", "");
        Assert.assertTrue(versionName == versionName1);
    }

    @Test
    public void getVersionName() {
        String versionName = PreferenceHelper.getVersionName(context);
        Assert.assertTrue(versionName == "");
    }
}