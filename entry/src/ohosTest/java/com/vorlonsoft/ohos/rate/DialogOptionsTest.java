package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.media.image.PixelMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * create date：2021/6/8 11:08
 *
 * @author huxi
 * description：
 */
public class DialogOptionsTest {

    private Context context;
    private DialogOptions dialogOptions;
    private Boolean cancelablePs;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        dialogOptions = new DialogOptions();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getCancelable() {
        Boolean cancelable = dialogOptions.getCancelable(context);
        Preferences preferencesEditor = new DatabaseHelper(context).getPreferences("rate_pref_set_file");
        cancelablePs = preferencesEditor.getBoolean("Cancelable",false);
        Assert.assertTrue( cancelable == cancelablePs);
    }

    @Test
    public void setCancelable() {
        Boolean can = dialogOptions.cancelable;
        dialogOptions.setCancelable(false);
        Assert.assertTrue(can == false);
    }

    @Test
    public void getRedrawn() {
        Boolean isRedrawn = dialogOptions.isRedrawn;
        Assert.assertTrue(isRedrawn == false);
    }

    @Test
    public void setRedrawn() {
        dialogOptions.setRedrawn(false);
        Boolean isRedrawn = dialogOptions.isRedrawn;
        Assert.assertTrue(isRedrawn == false);
    }

    @Test
    public void isShowIcon() {
        Boolean showIcon = dialogOptions.isShowIcon();
        Assert.assertTrue(showIcon != null);
    }

    @Test
    public void setShowIcon() {
        dialogOptions.setShowIcon(false);
        Boolean isShowIcon = dialogOptions.isShowIcon;
        Assert.assertTrue(isShowIcon == false);
    }

    @Test
    public void shouldShowMessage() {
        Boolean aBoolean = dialogOptions.shouldShowMessage();
        Assert.assertTrue(aBoolean == true);
    }

    @Test
    public void setShowMessage() {
        Boolean isShowMessage = dialogOptions.isShowMessage;
        dialogOptions.setShowMessage(true);
        Assert.assertTrue( isShowMessage == true);
    }

    @Test
    public void shouldShowNegativeButton() {
        Boolean aBoolean = dialogOptions.shouldShowNegativeButton();
        Assert.assertTrue( aBoolean == true);
    }

    @Test
    public void setShowNegativeButton() {
        dialogOptions.setShowNegativeButton(false);
        Boolean isShowNegativeButton = dialogOptions.isShowNegativeButton;
        Assert.assertTrue(isShowNegativeButton == false);
    }

    @Test
    public void shouldShowNeutralButton() {
        Boolean aBoolean = dialogOptions.shouldShowNeutralButton();
        Assert.assertTrue( aBoolean == true);
    }

    @Test
    public void setShowNeutralButton() {
        dialogOptions.setShowNeutralButton(false);
        Boolean isShowNegativeButton = dialogOptions.isShowNeutralButton;
        Assert.assertTrue(isShowNegativeButton == false);
    }

    @Test
    public void shouldShowTitle() {
        Boolean aBoolean = dialogOptions.shouldShowTitle(context);
        Assert.assertTrue( aBoolean == true);
    }

    @Test
    public void setShowTitle() {
        dialogOptions.setShowTitle(false);
        Boolean isShowNegativeButton = dialogOptions.isShowTitle;
        Assert.assertTrue(isShowNegativeButton == false);
    }

    @Test
    public void getCurrentRating() {
        Byte currentRating = dialogOptions.getCurrentRating();
        Assert.assertTrue( currentRating == 0);
    }

    @Test
    public void setCurrentRating() {
        Byte currentRating = 1;
        dialogOptions.setCurrentRating(currentRating);
        Byte b = dialogOptions.currentRating;
        Assert.assertTrue(b == 1);
    }

    @Test
    public void getIconResId() {
        int iconResId = dialogOptions.getIconResId();
        Assert.assertTrue( iconResId == 0);
    }

    @Test
    public void setNeutralTextResId() {
        int neutralTextResId = ResourceTable.String_rate_dialog_no;
        dialogOptions.setNeutralTextResId(neutralTextResId);
        int neutralTextResId1 = dialogOptions.neutralTextResId;
        Assert.assertTrue(neutralTextResId1 == neutralTextResId);
    }

    @Test
    public void getThemeResId() {
        Integer themeResId = dialogOptions.getThemeResId();
        Assert.assertTrue(themeResId == 0);
    }

    @Test
    public void setThemeResId() {
        Integer i= 1;
        dialogOptions.setThemeResId(i);
        Integer themeResId = dialogOptions.themeResId;
        Assert.assertTrue(themeResId == i);
    }

    @Test
    public void setTitleTextResId() {
        int titleTextResId = ResourceTable.String_rate_dialog_title;
        dialogOptions.setTitleTextResId(titleTextResId);
        int titleTextResId1 = dialogOptions.titleTextResId;
        Assert.assertTrue(titleTextResId1 == titleTextResId );
    }

    @Test
    public void getType() {
        int type = dialogOptions.getType();
        Assert.assertTrue(type ==  DialogType.CLASSIC);
    }

    @Test
    public void setType() {
        int type = DialogType.APPLE;
        dialogOptions.setType(type);
        int type1 = dialogOptions.type;
        Assert.assertTrue(type1 == type );
    }

    @Test
    public void getDialogType() {
        int dialogType = dialogOptions.getDialogType();
        Assert.assertTrue(dialogType == DialogType.CLASSIC);
    }

    @Test
    public void getMessageText() {
        String messageText = dialogOptions.getMessageText(context);
        Assert.assertNotNull(messageText);
    }

    @Test
    public void setMessageTextResId() {
        int i = ResourceTable.String_rate_dialog_now;
        dialogOptions.setMessageTextResId(i);
        int messageTextResId = dialogOptions.messageTextResId;
        Assert.assertNotNull(messageTextResId == i);
    }

    @Test
    public void getMessageResourse() {
        int i = ResourceTable.String_rate_dialog_now;
        Object messageResourse = dialogOptions.getMessageResourse(context, i);
        Assert.assertNotNull(messageResourse);
    }

    @Test
    public void setMessageText() {
        String messageText = "text";
        dialogOptions.setMessageText(messageText);
        String messageText1 = dialogOptions.messageText;
        Assert.assertTrue(messageText1 == messageText);
    }

    @Test
    public void getNegativeText() {
        String negativeText = dialogOptions.getNegativeText(context);
        Assert.assertNotNull(negativeText);
    }

    @Test
    public void setNegativeTextResId() {
        int negativeTextResId = ResourceTable.String_rate_dialog_cancel_en;
        dialogOptions.setNegativeTextResId(negativeTextResId);
        int negativeTextResId1 = dialogOptions.negativeTextResId;
        Assert.assertTrue(negativeTextResId1 == negativeTextResId);
    }

    @Test
    public void setNegativeText() throws NoSuchFieldException, IllegalAccessException {
        String messageText = "text";
        dialogOptions.setNegativeText(messageText);
        Field negativeText1 = dialogOptions.getClass().getDeclaredField("negativeText");
        negativeText1.setAccessible(true);
        String o = (String) negativeText1.get(dialogOptions);
        Assert.assertTrue(o == messageText );
    }

    @Test
    public void getNeutralText() {
        Object neutralText = dialogOptions.getNeutralText(context);
        Assert.assertNotNull(neutralText);
    }

    @Test
    public void setNeutralText() throws NoSuchFieldException, IllegalAccessException {
        String s = " String neutralText";
        dialogOptions.setNeutralText(s);
        Field neutralText = dialogOptions.getClass().getDeclaredField("neutralText");
        neutralText.setAccessible(true);
        String o = (String) neutralText.get(dialogOptions);
        Assert.assertTrue(o == s);
    }

    @Test
    public void getPositiveText() {
        String positiveText = dialogOptions.getPositiveText(context);
        Assert.assertNotNull(positiveText);
    }

    @Test
    public void setPositiveTextResId() {
        int id = ResourceTable.String_rate_dialog_cancel_en;
        dialogOptions.setPositiveTextResId(id);
        int positiveTextResId = dialogOptions.positiveTextResId;
        Assert.assertTrue(positiveTextResId == id);
    }

    @Test
    public void setPositiveText() throws NoSuchFieldException, IllegalAccessException {
        String positiveText = "positiveText";
        dialogOptions.setPositiveText(positiveText);
        Field positiveText1 = dialogOptions.getClass().getDeclaredField("positiveText");
        positiveText1.setAccessible(true);
        Object o = positiveText1.get(dialogOptions);
        Assert.assertTrue(o == positiveText);
    }

    @Test
    public void getTitleText() {
        Object titleText = dialogOptions.getTitleText(context);
        Assert.assertNotNull(titleText);
    }

    @Test
    public void setTitleText() throws NoSuchFieldException, IllegalAccessException {
        String titleText = " String titleText";
        dialogOptions.setTitleText(titleText);
        Field titleText1 = dialogOptions.getClass().getDeclaredField("titleText");
        titleText1.setAccessible(true);
        Object o = titleText1.get(dialogOptions);
        Assert.assertTrue(o == titleText);
    }

    @Test
    public void getButtonListener() {
        OnClickButtonListener buttonListener = dialogOptions.getButtonListener();
        Assert.assertNull(buttonListener);
    }

    @Test
    public void getIcon() {
        PixelMap icon = dialogOptions.getIcon(context);
        Assert.assertNotNull(icon);
    }

    @Test
    public void setIconResId() {
        int id = 10;
        dialogOptions.setIconResId(id);
        int iconResId = dialogOptions.iconResId;
        Assert.assertTrue(iconResId == id);
    }

    @Test
    public void setIcon() throws NoSuchFieldException, IllegalAccessException {
        dialogOptions.setIcon(null);
        Field icon1 = dialogOptions.getClass().getDeclaredField("icon");
        icon1.setAccessible(true);
        Object o = icon1.get(dialogOptions);
        Assert.assertTrue(o == null);
    }

    @Test
    public void getDialogIcon() {
        PixelMap dialogIcon = dialogOptions.getDialogIcon(context);
        Assert.assertNotNull(dialogIcon);
    }

    @Test
    public void getComponent() {
        Component component = dialogOptions.getComponent();
        Assert.assertNull(component);
    }

    @Test
    public void setComponent() throws NoSuchFieldException, IllegalAccessException {
        Component component = new Component(context);
        dialogOptions.setComponent(component);
        Field component1 = dialogOptions.getClass().getDeclaredField("component");
        component1.setAccessible(true);
        Object o = component1.get(dialogOptions);
        Assert.assertTrue(o == component);
    }
}