package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * create date：2021/6/8 11:02
 *
 * @author huxi
 * description：
 */
public class AppInformationTest {

    private AppInformation Ainformation;
    private Context context;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Ainformation = new AppInformation();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getIcon() throws NoSuchFieldException, IllegalAccessException {
        PixelMap icon = AppInformation.getIcon(context);
        Class<? extends AppInformation> aClass = Ainformation.getClass();
        Field icon1 = aClass.getDeclaredField("icon");
        icon1.setAccessible(true);
        PixelMap pixelMap = (PixelMap) icon1.get(Ainformation);
        Assert.assertEquals(pixelMap, icon);
    }

    @Test
    public void getLongVersionCode() {
        int longVersionCode = AppInformation.getLongVersionCode(context);
        Assert.assertTrue(longVersionCode != 0);
    }

    @Test
    public void getPackageName() throws NoSuchFieldException, IllegalAccessException {
        String packageName = AppInformation.getPackageName(context);
        Class<? extends AppInformation> aClass = Ainformation.getClass();
        Field name = aClass.getDeclaredField("packageName");
        name.setAccessible(true);
        String packageName1 = (String) name.get(Ainformation);
        Assert.assertEquals(packageName, packageName1);
    }

    @Test
    public void getVersionCodeMajor() throws NoSuchFieldException, IllegalAccessException {
        Integer versionCodeMajor = AppInformation.getVersionCodeMajor(context);
        Class<? extends AppInformation> aClass = Ainformation.getClass();
        Field name = aClass.getDeclaredField("versionCodeMajor");
        name.setAccessible(true);
        Integer versionCodeMajor1 = (Integer) name.get(Ainformation);
        Assert.assertEquals(versionCodeMajor, versionCodeMajor1);
    }

    @Test
    public void getVersionName() throws NoSuchFieldException, IllegalAccessException {
        String versionName = AppInformation.getVersionName(context);
        Class<? extends AppInformation> aClass = Ainformation.getClass();
        Field name = aClass.getDeclaredField("versionName");
        name.setAccessible(true);
        String versionName1 = (String) name.get(Ainformation);
        Assert.assertEquals(versionName, versionName1);
    }
}