package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * create date：2021/6/8 11:07
 *
 * @author huxi
 * description：
 */
public class DefaultDialogOnClickListenerTest {

    private Context context;
    private StoreOptions storeOptions =  new StoreOptions();

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getInstance() {
        DefaultDialogOnClickListener instance = DefaultDialogOnClickListener.getInstance(context, storeOptions, new OnClickButtonListener() {
            @Override
            public void onClickButton(Byte which) {

            }
        });
        Assert.assertNotNull(instance);
    }
}