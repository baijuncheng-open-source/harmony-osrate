package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * create date：2021/6/8 11:10
 *
 * @author huxi
 * description：
 */
public class RatingDialogTest {

    private Context context;
    private RatingDialog ratingDialog;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ratingDialog = new RatingDialog(context);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getContext() {
        Context contexts = RatingDialog.getContext();
        Assert.assertTrue(contexts == context);
    }

    @Test
    public void setOnShowListener() throws NoSuchFieldException, IllegalAccessException {
        DialogInterface.OnShowListener showListeners = new DialogInterface.OnShowListener() {
            @Override
            public void onShow(RatingDialog dialog) {

            }
        };
        ratingDialog.setOnShowListener(showListeners);
        Field showListener = ratingDialog.getClass().getDeclaredField("showListener");
        showListener.setAccessible(true);
        Object o = showListener.get(ratingDialog);
        Assert.assertNotNull(o);
    }

    @Test
    public void setOnDismissListener() throws NoSuchFieldException, IllegalAccessException {
        DialogInterface.OnDismissListener dismissListener = new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(RatingDialog dialog) {

            }
        };
        ratingDialog.setOnDismissListener(dismissListener);
        Field dismissListeners = ratingDialog.getClass().getDeclaredField("dismissListener");
        dismissListeners.setAccessible(true);
        Object o = dismissListeners.get(ratingDialog);
        Assert.assertNotNull(o);
    }
}