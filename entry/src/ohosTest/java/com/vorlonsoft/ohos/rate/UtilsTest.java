package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static com.vorlonsoft.ohos.rate.Constants.Utils.EMPTY_STRING_ARRAY;

/**
 * create date：2021/6/8 11:11
 *
 * @author huxi
 * description：
 */
public class UtilsTest {

    private Context context;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getDialogBuilder() {
        int i = 0;
        RatingDialog dialogBuilder = Utils.getDialogBuilder(context, i);
        Assert.assertNotNull(dialogBuilder);
    }

    @Test
    public void getCommonDialog() {
        RatingDialog dialogBuilder = Utils.getCommonDialog(context);
        Assert.assertNotNull(dialogBuilder);
    }

    @Test
    public void isPackagesExists() {
        ArrayList<String> storesPackagesName = new ArrayList<>();
        ArrayList<String> packagesExists = Utils.isPackagesExists(context, storesPackagesName);
        Assert.assertTrue(packagesExists == EMPTY_STRING_ARRAY);
    }

    @Test
    public void testIsPackagesExists() {
        ArrayList<String> storesPackagesName = new ArrayList<>();
        storesPackagesName.add("com.huawei.appmarket");
        ArrayList<String> packagesExists = Utils.isPackagesExists(context, storesPackagesName);
        String s = packagesExists.get(0);
        Assert.assertTrue(s == "com.huawei.appmarket");
    }

    @Test
    public void getResIdByName() {
        String name = context.getApplicationInfo().getIcon();
        String defType = "Media";
        String defTypes = "Element";
        int resId = Utils.getResIdByName(name, defType);
        Assert.assertTrue(resId != -1);
        int resIds = Utils.getResIdByName(name, defTypes);
        Assert.assertTrue(resIds == -1);
    }
}