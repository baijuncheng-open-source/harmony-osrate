package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.window.dialog.BaseDialog;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

/**
 * create date：2021/6/8 10:47
 *
 * @author huxi
 * description：
 */
public class AppCompatDialogManagerTest {
    private AppCompatDialogManager appCompatDialogManager;
    private Context context;
    private DialogOptions dialogOptions =  new DialogOptions();
    private StoreOptions storeOptions =  new StoreOptions();
    private MainAbility ability;

    @Before
    public void setUp() throws Exception {

        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        appCompatDialogManager = new AppCompatDialogManager(context, dialogOptions, storeOptions);
        Intent intent=new Intent();
        Operation operation = new Intent.OperationBuilder()
                                      .withDeviceId("")
                                      .withBundleName("com.vorlonsoft.ohos.rate")
                                      .withAbilityName("com.vorlonsoft.ohos.rate.MainAbility")
                                      .build();
        intent.setOperation(operation);
        ability= (MainAbility) AbilityDelegatorRegistry.getAbilityDelegator().startAbilitySync(intent).get();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getAppCompatDialogBuilder() {
        RatingDialog appCompatDialogBuilder = appCompatDialogManager.getAppCompatDialogBuilder(ability,
                0);
        Assert.assertNotNull(appCompatDialogBuilder);
    }

    @Test
    public void supplyAppCompatClassicDialogArguments() {
        RatingDialog ratingDialog = new RatingDialog(ability);
        dialogOptions.isShowIcon = true;
        dialogOptions.isShowTitle = true;
        dialogOptions.isShowMessage = true;
        dialogOptions.isShowNeutralButton = true;
        appCompatDialogManager.supplyAppCompatClassicDialogArguments(ratingDialog, ability);
        Assert.assertTrue(ratingDialog.isShowing());
    }

    @Test
    public void createDialog() {
        appCompatDialogManager.setContext(ability);
        BaseDialog dialog = appCompatDialogManager.createDialog();
        Assert.assertNotNull(dialog);
    }

    @Test
    public void clearDialogManager() throws NoSuchFieldException, IllegalAccessException {
        AppCompatDialogManager.Factory factory = new AppCompatDialogManager.Factory();
        DialogManager dialogManager = factory.createDialogManager(ability, dialogOptions, storeOptions);
        factory.clearDialogManager();
        Class<? extends AppCompatDialogManager> aClass = appCompatDialogManager.getClass();
        Field name = aClass.getDeclaredField("singleton");
        name.setAccessible(true);
        WeakReference<DialogManager> reference = (WeakReference<DialogManager>) name.get(appCompatDialogManager);
        Assert.assertNull(reference.get());
    }

    @Test
    public void createDialogManager() {
        AppCompatDialogManager.Factory factory = new AppCompatDialogManager.Factory();
        DialogManager dialogManager = factory.createDialogManager(ability, dialogOptions, storeOptions);
        Assert.assertNotNull(dialogManager);
    }
}