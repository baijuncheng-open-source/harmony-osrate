package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static com.vorlonsoft.ohos.rate.DialogInterface.BUTTON_NEUTRAL;
import static com.vorlonsoft.ohos.rate.StoreType.*;

/**
 * create date：2021/6/8 11:06
 *
 * @author huxi
 * description：
 */
public class AppRateTest {

    private Context context;
    private MainAbility ability;
    private AbilitySlice slice;
    private AppRate appRate;
    private Field dialogOptionsField ;
    private Field storeOptionsField ;
    private DialogOptions dialogOptions ;
    private StoreOptions storeoptions ;
    private Preferences preferencesEditors;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                                      .withDeviceId("")
                                      .withBundleName("com.vorlonsoft.ohos.rate")
                                      .withAbilityName("com.vorlonsoft.ohos.rate.MainAbility")
                                      .build();
        intent.setOperation(operation);
        ability = (MainAbility) AbilityDelegatorRegistry.getAbilityDelegator().startAbilitySync(intent).get();
        slice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        appRate = AppRate.with(context);
        Field singleton = appRate.getClass().getDeclaredField("singleton");
        singleton.setAccessible(true);
        singleton.set(appRate, null);
        dialogOptionsField = appRate.getClass().getDeclaredField("dialogOptions");
        dialogOptionsField.setAccessible(true);
        dialogOptions = (DialogOptions) dialogOptionsField.get(appRate);
        storeOptionsField = appRate.getClass().getDeclaredField("storeOptions");
        storeOptionsField.setAccessible(true);
        storeoptions = (StoreOptions) storeOptionsField.get(appRate);
        preferencesEditors = new DatabaseHelper(context).getPreferences("rate_pref_file");
    }

    @After
    public void tearDown() throws Exception {
        Field singleton = appRate.getClass().getDeclaredField("singleton");
        singleton.setAccessible(true);
        singleton.set(null,null);
        preferencesEditors.clear();
    }

    @Test
    public void with() {
        AppRate with = AppRate.with(context);
        Assert.assertNotNull(with);
    }

    @Test
    public void showRateDialogIfMeetsConditions_true() {
        Assert.assertTrue(AppRate.with(context).setDebug(true).showRateDialogIfMeetsConditions(slice));
    }

    @Test
    public void showRateDialogIfMeetsConditions_false() {
        Assert.assertFalse(AppRate.showRateDialogIfMeetsConditions(slice));
    }

    @Test
    public void quickStart() {
        Assert.assertFalse(AppRate.quickStart(slice));
    }

    @Test
    public void set365DayPeriodMaxNumberDialogLaunchTimes() throws IllegalAccessException, NoSuchFieldException {
        appRate.set365DayPeriodMaxNumberDialogLaunchTimes((short) 100);
        Field dialogLaunchTimes = appRate.getClass().getDeclaredField("dialogLaunchTimes");
        dialogLaunchTimes.setAccessible(true);
        short time = (short) dialogLaunchTimes.get(appRate);
        Assert.assertTrue(time == 100);
    }

    @Test
    public void setLaunchTimes() throws NoSuchFieldException, IllegalAccessException {
        appRate.setLaunchTimes((byte) 10);
        Field launchtimes = appRate.getClass().getDeclaredField("appLaunchTimes");
        launchtimes.setAccessible(true);
        byte time = (byte) launchtimes.get(appRate);
        Assert.assertTrue(time == 10);
    }

    @Test
    public void setInstallDays() throws NoSuchFieldException, IllegalAccessException {
        appRate.setInstallDays((byte) 3);
        Field installDate = appRate.getClass().getDeclaredField("installDate");
        installDate.setAccessible(true);
        long time = (long) installDate.get(appRate);
        Assert.assertTrue(time == Time.DAY * 3L);
    }

    @Test
    public void setTimeToWait() throws NoSuchFieldException, IllegalAccessException {
        appRate.setTimeToWait(Time.DAY,(byte) 3);
        Field installDate = appRate.getClass().getDeclaredField("installDate");
        installDate.setAccessible(true);
        long time = (long) installDate.get(appRate);
        Assert.assertTrue(time == Time.DAY * 3L);
    }

    @Test
    public void setRemindInterval() throws NoSuchFieldException, IllegalAccessException {
        appRate.setRemindInterval((byte) 3);
        Field remindInterval = appRate.getClass().getDeclaredField("remindInterval");
        remindInterval.setAccessible(true);
        long time = (long) remindInterval.get(appRate);
        Assert.assertTrue(time == Time.DAY * 3L);
    }

    @Test
    public void setRemindTimeToWait() throws NoSuchFieldException, IllegalAccessException {
        appRate.setRemindTimeToWait(Time.DAY,(byte) 3);
        Field remindInterval = appRate.getClass().getDeclaredField("remindInterval");
        remindInterval.setAccessible(true);
        long time = (long) remindInterval.get(appRate);
        Assert.assertTrue(time == Time.DAY * 3L);
    }

    @Test
    public void setRemindLaunchesNumber() throws NoSuchFieldException, IllegalAccessException {
        appRate.setRemindLaunchesNumber((byte) 10);
        Field remindLaunchesNumber = appRate.getClass().getDeclaredField("remindLaunchesNumber");
        remindLaunchesNumber.setAccessible(true);
        byte time = (byte) remindLaunchesNumber.get(appRate);
        Assert.assertTrue(time == 10);
    }

    @Test
    public void clearRemindButtonClick() {
        Preferences preferences = new DatabaseHelper(context).getPreferences("rate_pref_file");
        long l = preferences.getLong("rate_remind_interval",0);
        int i =preferences.getInt("rate_remind_launches_number",0);
        appRate.clearRemindButtonClick();
        Assert.assertEquals(l,0);
        Assert.assertEquals(i,0);
    }

    @Test
    public void setMinimumEventCount() throws NoSuchFieldException, IllegalAccessException {
        appRate.setMinimumEventCount("one", (short) 3);
        Field minimumEventCount = appRate.getClass().getDeclaredField("customEventsCounts");
        minimumEventCount.setAccessible(true);
        Map<String, Short> map = (Map<String, Short>) minimumEventCount.get(appRate);
        Map<String, Short> map1 = new HashMap<>();
        map1.put("one", (short) 3);
        Assert.assertEquals(map1,map);
    }

    @Test
    public void setSelectedAppLaunches() throws NoSuchFieldException, IllegalAccessException {
        appRate.setSelectedAppLaunches((byte) 10);
        Field selectedAppLaunches = appRate.getClass().getDeclaredField("selectedAppLaunches");
        selectedAppLaunches.setAccessible(true);
        byte time = (byte) selectedAppLaunches.get(appRate);
        Assert.assertTrue(time == 10);
    }

    @Test
    public void setRemindLaunchTimes() throws NoSuchFieldException, IllegalAccessException {
        appRate.setRemindLaunchTimes((byte) 12);
        Field selectedAppLaunches = appRate.getClass().getDeclaredField("selectedAppLaunches");
        selectedAppLaunches.setAccessible(true);
        byte time = (byte) selectedAppLaunches.get(appRate);
        Assert.assertTrue(time == 12);
    }

    @Test
    public void setShowLaterButton() {
        appRate.setShowLaterButton(true);
        Boolean isShowNeutralButton = dialogOptions.isShowNeutralButton;
        Assert.assertTrue(isShowNeutralButton == true);
    }

    @Test
    public void setShowNeverButton() throws NoSuchFieldException, IllegalAccessException {
        appRate.setShowNeverButton(true);
        Boolean isShowNegativeButton1 = dialogOptions.isShowNegativeButton;
        Assert.assertTrue(isShowNegativeButton1 == true);
    }

    @Test
    public void setShowTitle() {
        appRate.setShowTitle(true);
        boolean b = dialogOptions.isShowTitle;
        Assert.assertTrue(b == true);
    }


    @Test
    public void setShowMessage() {
        appRate.setShowMessage(true);
        boolean b = dialogOptions.isShowMessage;
        Assert.assertTrue(b == true);
    }

    @Test
    public void setShowDialogIcon() {
        appRate.setShowDialogIcon(true);
        boolean b = dialogOptions.isShowIcon;
        Assert.assertTrue(b == true);
    }

    @Test
    public void clearSettingsParam() {
        Preferences preferences =new DatabaseHelper(context).getPreferences("rate_pref_file");
        long l = preferences.getLong("rate_dialog_first_launch_time",0);
        appRate.clearSettingsParam();
        Assert.assertTrue(l == 0);
    }

    @Test
    public void clearAgreeShowDialog() {
        Preferences preferences = new DatabaseHelper(context).getPreferences("rate_pref_file");
        boolean b = preferences.getBoolean("rate_is_agree_show_dialog",true);
        appRate.clearAgreeShowDialog();
        Assert.assertTrue(b == true);
    }

    @Test
    public void getAgreeShowDialog() {
        Preferences preferences =new DatabaseHelper(context).getPreferences("rate_pref_file");
        boolean b = preferences.getBoolean("rate_is_agree_show_dialog",true);
        appRate.getAgreeShowDialog();
        Assert.assertTrue(b == true);
    }

    @Test
    public void setAgreeShowDialog() {
        appRate.setAgreeShowDialog(false);
        Preferences preferences =new DatabaseHelper(context).getPreferences("rate_pref_file");
        boolean b = preferences.getBoolean("rate_is_agree_show_dialog",false);
        Assert.assertTrue(b == false);
    }

    @Test
    public void setOnClickButtonListener() {
        ToastDialog toastDialog = new ToastDialog(context);
        appRate.setOnClickButtonListener(new OnClickButtonListener() {
            @Override
            public void onClickButton(Byte which) {
                switch (which){
                    case BUTTON_NEUTRAL:
                        toastDialog.setText("点击了不，谢谢").setDuration(2000).show();
                }
            }
        });
        SoftReference<OnClickButtonListener> buttonListener = dialogOptions.buttonListener;
        Assert.assertNotNull(buttonListener);
    }

    @Test
    public void setDialogIcon() throws NoSuchFieldException, IllegalAccessException {
        appRate.setDialogIcon(ResourceTable.Media_rate_dialog_star_unfilled);
        int iconResId = dialogOptions.iconResId;
        Assert.assertTrue(iconResId == ResourceTable.Media_rate_dialog_star_unfilled );
    }

    @Test
    public void testSetDialogIcon() throws IOException, NotExistException, NoSuchFieldException, IllegalAccessException {
        Resource resource = context.getResourceManager().getResource(ResourceTable.Media_rate_dialog_star_unfilled);
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        ImageSource imageSource = ImageSource.create(resource, srcOpts);
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        PixelMap pixelmap = imageSource.createPixelmap(decodingOptions);
        appRate.setDialogIcon(pixelmap);
        Field icon = dialogOptions.getClass().getDeclaredField("icon");
        icon.setAccessible(true);
        PixelMap icons = (PixelMap) icon.get(dialogOptions);
        Assert.assertTrue(icons == pixelmap);
    }

    @Test
    public void setTitle() {
        appRate.setTitle(ResourceTable.String_TITLE);
        int i = dialogOptions.titleTextResId;
        Assert.assertTrue(i == ResourceTable.String_TITLE );
    }

    @Test
    public void testSetTitle() throws NoSuchFieldException, IllegalAccessException {
        String title = "Title";
        appRate.setTitle(title);
        Field titleText = dialogOptions.getClass().getDeclaredField("titleText");
        titleText.setAccessible(true);
        String s = (String) titleText.get(dialogOptions);
        Assert.assertTrue(s == title );
    }

    @Test
    public void setMessage() {
        appRate.setMessage(ResourceTable.String_new_rate_dialog_message);
        int i = dialogOptions.messageTextResId;
        Assert.assertTrue(i == ResourceTable.String_new_rate_dialog_message );
    }

    @Test
    public void testSetMessage() throws NoSuchFieldException, IllegalAccessException {
        String message = "Message";
        appRate.setMessage(message);
        Field messageText = dialogOptions.getClass().getDeclaredField("messageText");
        messageText.setAccessible(true);
        String s = (String) messageText.get(dialogOptions);
        Assert.assertTrue(s == message );
    }

    @Test
    public void setTextRateNow() {
        appRate.setTextRateNow(ResourceTable.String_new_rate_dialog_never);
        int i = dialogOptions.positiveTextResId;
        Assert.assertTrue(i == ResourceTable.String_new_rate_dialog_never );
    }

    @Test
    public void testSetTextRateNow() throws NoSuchFieldException, IllegalAccessException {
        String Text = "positiveText";
        appRate.setTextRateNow(Text);
        Field positiveText = dialogOptions.getClass().getDeclaredField("positiveText");
        positiveText.setAccessible(true);
        String s = (String) positiveText.get(dialogOptions);
        Assert.assertTrue(s == Text );
    }

    @Test
    public void setTextLater() {
        appRate.setTextLater(ResourceTable.String_new_rate_dialog_never);
        int i = dialogOptions.neutralTextResId;
        Assert.assertTrue(i == ResourceTable.String_new_rate_dialog_never );
    }

    @Test
    public void testSetTextLater() throws NoSuchFieldException, IllegalAccessException {
        String Text = "neutralText";
        appRate.setTextLater(Text);
        Field neutralText = dialogOptions.getClass().getDeclaredField("neutralText");
        neutralText.setAccessible(true);
        String s = (String) neutralText.get(dialogOptions);
        Assert.assertTrue(s == Text );
    }

    @Test
    public void setTextNever() {
        appRate.setTextNever(ResourceTable.String_new_rate_dialog_never);
        int i = dialogOptions.negativeTextResId;
        Assert.assertTrue(i == ResourceTable.String_new_rate_dialog_never );
    }

    @Test
    public void testSetTextNever() throws NoSuchFieldException, IllegalAccessException {
        String Text = "neutralText";
        appRate.setTextNever(Text);
        Field negativeText = dialogOptions.getClass().getDeclaredField("negativeText");
        negativeText.setAccessible(true);
        String s = (String) negativeText.get(dialogOptions);
        Assert.assertTrue(s == Text );
    }

    @Test
    public void setCancelable() {
        appRate.setCancelable(false);
        boolean i = dialogOptions.cancelable;
        Assert.assertTrue(i == false );
    }

    @Test
    public void setStoreType() throws NoSuchFieldException, IllegalAccessException {
        appRate.setStoreType(CHINESESTORES);
        Field storeType = storeoptions.getClass().getDeclaredField("storeType");
        storeType.setAccessible(true);
        int s = (int) storeType.get(storeoptions);
        Assert.assertTrue(s ==  CHINESESTORES);
    }

    @Test
    public void testSetStoreType() throws NoSuchFieldException, IllegalAccessException {
        appRate.setStoreType(CHINESESTORES,2);
        Field storeType = storeoptions.getClass().getDeclaredField("storeType");
        storeType.setAccessible(true);
        int s = (int) storeType.get(storeoptions);
        Assert.assertTrue(s ==  CHINESESTORES);
    }

    @Test
    public void testSetStoreType1() throws NoSuchFieldException, IllegalAccessException {
        String uri = "www";
        appRate.setStoreType(uri);
        Field storeType = storeoptions.getClass().getDeclaredField("storeType");
        storeType.setAccessible(true);
        int s = (int) storeType.get(storeoptions);
        Assert.assertTrue(s ==  OTHER);
    }

    @Test
    public void testSetStoreType2() throws NoSuchFieldException, IllegalAccessException {
        Intent intent = new Intent();
        appRate.setStoreType(intent);
        Field storeType = storeoptions.getClass().getDeclaredField("storeType");
        storeType.setAccessible(true);
        int s = (int) storeType.get(storeoptions);
        Assert.assertTrue(s ==  INTENT);
    }

    @Test
    public void getStoreType() throws NoSuchFieldException, IllegalAccessException {
        appRate.getStoreType();
        Field storeType = storeoptions.getClass().getDeclaredField("storeType");
        storeType.setAccessible(true);
        int s = (int) storeType.get(storeoptions);
        Assert.assertTrue(s ==  CHINESESTORES);
    }

    @Test
    public void incrementEventCount() {
        String name ="eventname";
        appRate.incrementEventCount(name);
        int customEventPrefix = preferencesEditors.getInt("rate_custom_event_prefix_", 0);
        Assert.assertTrue(customEventPrefix ==  0);
    }

    @Test
    public void setEventCountValue() {
        String name ="eventname";
        short i = 3;
        appRate.setEventCountValue(name, i);
        int customEventPrefix = preferencesEditors.getInt("rate_custom_event_prefix_", 0);
        Assert.assertTrue(customEventPrefix ==  0);
    }

    @Test
    public void setThemeResId() {
        appRate.setThemeResId(ResourceTable.String_new_rate_dialog_message);
        int i = dialogOptions.themeResId;
        Assert.assertTrue(i == ResourceTable.String_new_rate_dialog_message );
    }

    @Test
    public void setVersionCodeCheck() throws NoSuchFieldException, IllegalAccessException {
        appRate.setVersionCodeCheck(false);
        Field isVersionCodeCheck = appRate.getClass().getDeclaredField("isVersionCodeCheck");
        isVersionCodeCheck.setAccessible(true);
        boolean b = (boolean) isVersionCodeCheck.get(appRate);
        Assert.assertTrue(b == false );
    }

    @Test
    public void setVersionNameCheck() throws NoSuchFieldException, IllegalAccessException {
        appRate.setVersionNameCheck(false);
        Field isVersionNameCheck = appRate.getClass().getDeclaredField("isVersionNameCheck");
        isVersionNameCheck.setAccessible(true);
        boolean b = (boolean) isVersionNameCheck.get(appRate);
        Assert.assertTrue(b == false );
    }

    @Test
    public void monitor() {
        preferencesEditors.putLong("rate_install_date_",0L);
        appRate.monitor();
        int launchTimes = preferencesEditors.getInt("rate_launch_times", 0);
        Assert.assertTrue(launchTimes == 0);
    }

    @Test
    public void testMonitor() {
        preferencesEditors.putLong("rate_install_date_",2L);
        appRate.monitor();
        boolean isAgreeShowDialog = preferencesEditors.getBoolean("PREF_KEY_IS_AGREE_SHOW_DIALOG", true);
        Assert.assertTrue(isAgreeShowDialog == true);
    }

    @Test
    public void dismissRateDialog() throws NoSuchFieldException, IllegalAccessException {
        appRate.dismissRateDialog();
        Field dialog = appRate.getClass().getDeclaredField("dialog");
        dialog.setAccessible(true);
        WeakReference<BaseDialog> o = (WeakReference<BaseDialog>) dialog.get(appRate);
        Assert.assertNull(o);
    }

    @Test
    public void rateNow() throws NoSuchFieldException, IllegalAccessException {
        appRate.rateNow(slice);
        Field dialog = appRate.getClass().getDeclaredField("dialog");
        dialog.setAccessible(true);
        WeakReference<BaseDialog> o = (WeakReference<BaseDialog>) dialog.get(appRate);
        Assert.assertNull(o);
    }

    @Test
    public void shouldShowRateDialog() {
        appRate.shouldShowRateDialog();
        boolean isAgreeShowDialog = preferencesEditors.getBoolean("rate_is_agree_show_dialog", true);
        Assert.assertTrue(isAgreeShowDialog == true);
    }

    @Test
    public void isDebug() throws NoSuchFieldException, IllegalAccessException {
        appRate.isDebug();
        Field isDebug = appRate.getClass().getDeclaredField("isDebug");
        isDebug.setAccessible(true);
        boolean b = (boolean) isDebug.get(appRate);
        Assert.assertTrue(b == false);
    }

    @Test
    public void setDebug() throws NoSuchFieldException, IllegalAccessException {
        appRate.setDebug(true);
        Field isDebug = appRate.getClass().getDeclaredField("isDebug");
        isDebug.setAccessible(true);
        boolean b = (boolean) isDebug.get(appRate);
        Assert.assertTrue(b == true);
    }
}