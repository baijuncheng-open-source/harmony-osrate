package com.vorlonsoft.ohos.rate;

import ohos.aafwk.content.Intent;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static com.vorlonsoft.ohos.rate.StoreType.*;

/**
 * create date：2021/6/8 11:10
 *
 * @author huxi
 * description：
 */
public class StoreOptionsTest {

    private StoreOptions storeOptions;
    private String o;

    @Before
    public void setUp() throws Exception {
        storeOptions = new StoreOptions();
        Field applicationId = storeOptions.getClass().getDeclaredField("applicationId");
        applicationId.setAccessible(true);
        o = (String) applicationId.get(storeOptions);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getApplicationId()  {
        storeOptions.getApplicationId();
        Assert.assertTrue(o == null);
    }

    @Test
    public void getIntents() throws NoSuchFieldException, IllegalAccessException {
        storeOptions.getIntents();
        Field intents = storeOptions.getClass().getDeclaredField("intents");
        intents.setAccessible(true);
        Intent[] o = (Intent[]) intents.get(storeOptions);
        Assert.assertTrue(o == null);
    }

    @Test
    public void getStoreType() throws NoSuchFieldException, IllegalAccessException {
        storeOptions.getStoreType();
        Field storeType = storeOptions.getClass().getDeclaredField("storeType");
        storeType.setAccessible(true);
        int o = (int) storeType.get(storeOptions);
        Assert.assertTrue(o == GOOGLEPLAY);
    }

    @Test
    public void setStoreType() throws NoSuchFieldException, IllegalAccessException {
        int storeType = BLACKBERRY;
        String[] stringParam = new String[]{"0","1","2","3"};
        Intent[] intentParam = null;
        storeOptions.setStoreType(storeType,stringParam,intentParam);
        Field applicationId = storeOptions.getClass().getDeclaredField("applicationId");
        applicationId.setAccessible(true);
        String o = (String) applicationId.get(storeOptions);
        Assert.assertTrue(o == "0");
        int storeTypes = INTENT;
        storeOptions.setStoreType(storeTypes,stringParam,intentParam);
        Field intents = storeOptions.getClass().getDeclaredField("intents");
        intents.setAccessible(true);
        Object o1 = intents.get(storeOptions);
        Assert.assertTrue(o1 == null);
        int storeType1 = OTHER;
        storeOptions.setStoreType(storeType1,stringParam,intentParam);
        Field intent1 = storeOptions.getClass().getDeclaredField("intents");
        intent1.setAccessible(true);
        Object o2 = intent1.get(storeOptions);
        Assert.assertTrue(o1 == null);
    }
}