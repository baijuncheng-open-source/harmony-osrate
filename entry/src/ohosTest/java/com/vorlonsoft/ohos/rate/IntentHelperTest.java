package com.vorlonsoft.ohos.rate;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * create date：2021/6/8 11:09
 *
 * @author huxi
 * description：
 */
public class IntentHelperTest {

    private Context context;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testCreateIntentsForStore() {
        int appStore = StoreType.GOOGLEPLAY;
        String packageName = AppInformation.getPackageName(context);
        int length = IntentHelper.createIntentsForStore(context, appStore, packageName).length;
        int i = new int[2].length;
        Assert.assertTrue(length == i);
    }
}