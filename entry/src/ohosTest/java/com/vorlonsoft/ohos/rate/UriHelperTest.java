package com.vorlonsoft.ohos.rate;

import ohos.utils.net.Uri;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.vorlonsoft.ohos.rate.StoreType.*;

/**
 * create date：2021/6/8 11:10
 *
 * @author huxi
 * description：
 */
public class UriHelperTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getStoreUri() {
        String paramName = "com";
        String storeUri1 = UriHelper.getStoreUri(AMAZON, paramName).toString();
        Assert.assertEquals(storeUri1,"amzn://apps/android?p=com");
        Uri storeUri2 = UriHelper.getStoreUri(APPLE, paramName);
        Assert.assertTrue(storeUri2 == null);
        String storeUri3 = UriHelper.getStoreUri(BAZAAR, paramName).toString();
        Assert.assertEquals(storeUri3,"bazaar://details?id=com");
        String storeUri4 = UriHelper.getStoreUri(BLACKBERRY, paramName).toString();
        Assert.assertEquals(storeUri4,"appworld://content/com");
        String storeUri5 = UriHelper.getStoreUri(CHINESESTORES, paramName).toString();
        Assert.assertEquals(storeUri5,"market://details?id=com");
        String storeUri6 = UriHelper.getStoreUri(MI, paramName).toString();
        Assert.assertEquals(storeUri6,"market://details?id=com");
        String storeUri7 = UriHelper.getStoreUri(SAMSUNG, paramName).toString();
        Assert.assertEquals(storeUri7,"samsungapps://ProductDetail/com");
        String storeUri8 = UriHelper.getStoreUri(SLIDEME, paramName).toString();
        Assert.assertEquals(storeUri8,"sam://details?id=com");
        String storeUri9 = UriHelper.getStoreUri(TENCENT, paramName).toString();
        Assert.assertEquals(storeUri9,"market://details?id=com");
        String storeUri10 = UriHelper.getStoreUri(YANDEX, paramName).toString();
        Assert.assertEquals(storeUri10,"yastore://details?id=com");
        String storeUri11 = UriHelper.getStoreUri(OTHER, paramName).toString();
        Assert.assertEquals(storeUri11,"market://details?id=com");
    }

    @Test
    public void getStoreWebUri() {
        String paramName = "cn";
        String storeWebUri1 = UriHelper.getStoreWebUri(AMAZON, paramName).toString();
        Assert.assertEquals(storeWebUri1,"https://www.amazon.com/gp/mas/dl/android?p=cn");
        String storeWebUri2 = UriHelper.getStoreWebUri(APPLE, paramName).toString();
        Assert.assertEquals(storeWebUri2,"https://itunes.apple.com/app/idcn");
        String storeWebUri3 = UriHelper.getStoreWebUri(BAZAAR, paramName).toString();
        Assert.assertEquals(storeWebUri3,"https://cafebazaar.ir/app/cn");
        String storeWebUri4 = UriHelper.getStoreWebUri(BLACKBERRY, paramName).toString();
        Assert.assertEquals(storeWebUri4,"https://appworld.blackberry.com/webstore/content/cn");
        Uri storeWebUri5 = UriHelper.getStoreWebUri(CHINESESTORES, paramName);
        Assert.assertTrue(storeWebUri5 == null);
        String storeWebUri6 = UriHelper.getStoreWebUri(MI, paramName).toString();
        Assert.assertEquals(storeWebUri6,"http://app.xiaomi.com/details?id=cn");
        String storeWebUri7 = UriHelper.getStoreWebUri(SAMSUNG, paramName).toString();
        Assert.assertEquals(storeWebUri7,"https://apps.samsung.com/appquery/appDetail.as?appId=cn");
        String storeWebUri8 = UriHelper.getStoreWebUri(SLIDEME, paramName).toString();
        Assert.assertEquals(storeWebUri8, "http://slideme.org/app/cn");
        String storeWebUri9 = UriHelper.getStoreWebUri(TENCENT, paramName).toString();
        Assert.assertEquals(storeWebUri9, "http://a.app.qq.com/o/simple.jsp?pkgname=cn");
        String storeWebUri10 = UriHelper.getStoreWebUri(YANDEX, paramName).toString();
        Assert.assertEquals(storeWebUri10, "https://store.yandex.com/apps/details?id=cn");
        String storeWebUri11 = UriHelper.getStoreWebUri(OTHER, paramName).toString();
        Assert.assertEquals(storeWebUri11,  "https://play.google.com/store/apps/details?id=cn");
    }
}