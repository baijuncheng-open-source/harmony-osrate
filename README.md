### 简介
>HarmonyOSRate是一个评分库，可通过提示用户在使用几天后对其进行评分来帮助您推广HarmonyOS应用。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0709/155403_ff6cd2f1_8230582.gif "HarmonyOSRate.gif")

###  **使用**

1.直接导入源码使用
一、下载源码后，将源码中的library模块copy到自己项目中，与entry同级。
       将library作为一个模块，参照源码demo，与自己的其他模块相关联。
	
二、要弹出提示对话框，参照一下调用方法。

       1. 监视应用程序启动时间，并在满足默认条件时显示速率对话框
          AppRate.quickStart(this); 
	
        2.  仅配置基本参数，弹出提示对话框
         AppRate.with(this)
                      .setTimeToWait(Time.DAY, (short) 0)
                      .setLaunchTimes((byte) 0)
                      .setSelectedAppLaunches((byte) 1)
                      .monitor();

            AppRate.showRateDialogIfMeetsConditions(this);

        3.强制弹出提示对话框
          AppRate.with(this).showRateDialog(this)；

      对话框的默认选项如下：
        1.当您按下评分按钮时候，Google Play就会启动，改变默认值可以通过AppRate.setStoreType(int), AppRate.setStoreType(int, long), 
          AppRate.setStoreType(String...) or AppRate.setStoreType(Intent...)设置.
        2.应用程序比安装晚10天或更多天启动。可以通过AppRate.setTimeToWait(long, short)或AppRate.setInstallDays(byte)进行更改。
        3.单击中性按钮后1天或更长时间启动应用程序。通过AppRate.setRemindTimeToWait(long, short)或AppRate.setRemindInterval(byte)进行更改。
        4.设置启动频率，可以通过AppRate#setSelectedAppLaunches(byte)进行更改（如果appLaunches％param== 0，则满足条件）。
        5.应用程序显示“中性”按钮（“稍后提醒我”）。可以通过setShowLaterButton(boolean)进行更改。
        6.在365天内设置最大次数地显示对话框，默认值无限制。可以通过AppRate#set365DayPeriodMaxNumberDialogLaunchTimes(short)进行更改。
        7.设置AppRate.setDebug(boolean)以true确保每次启动应用程序时都将显示“速率”对话框。此功能仅用于开发！。
        8.按下对话框按钮时，没有默认的回调。通过进行更改AppRate.with(this).setOnClickButtonListener(OnClickButtonListener)。

2.搭配如har包使用
   将项目运行后生成的har包，放入到自己的lib中，添加完依赖后，使用方法导入源码中调用即可。

3.远程依赖

dependencies {
    
    implementation 'com.gitee.baijuncheng-open-source:harmony-osrate:1.0.1'
}


License

The MIT License (MIT)

Copyright (c) 2017 - 2018 Vorlonsoft LLC

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.